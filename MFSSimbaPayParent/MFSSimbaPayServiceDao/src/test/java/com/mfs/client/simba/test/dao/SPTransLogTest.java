package com.mfs.client.simba.test.dao;

import java.util.Date;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dao.LogTransSessionDao;
import com.mfs.client.simba.model.SPTransSession;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class SPTransLogTest {

	@Autowired
	LogTransSessionDao logTransSessionDao;
	
	@Test
	@Ignore
	public void logTransSession()
	{
		SPTransSession spTransSession=new SPTransSession();
		spTransSession.setQuoteReference("Quote102");
		spTransSession.setMfsTransId("MFS001");
		spTransSession.setTransactionReference("TXNREF200");
		spTransSession.setCallBackUrl("/callbackurl");
		spTransSession.setSenderFirstName("Shubham");
		spTransSession.setSenderLastName("Shah");
		spTransSession.setSenderMobile("7856565656");
		spTransSession.setSenderPostcode("232084");
		spTransSession.setSenderCountry("IND");
		spTransSession.setRecipientFirstName("Ankita");
		spTransSession.setRecipientLastName("Bide");
		spTransSession.setRecipientMobile("8967676776");
		spTransSession.setRecipientCountry("AUS");
		spTransSession.setAccountNumber("SBI2345678");
		spTransSession.setBankCode("2001");
		spTransSession.setStatus("Initiated");
		spTransSession.setMessage("Transaction Initiated Successfully");
		spTransSession.setDate(new Date());
		long result = logTransSessionDao.logTransSession(spTransSession);
		Assert.assertEquals(1, result);
	}
	
}
