package com.mfs.client.test.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetRateResponseDto;
import com.mfs.client.simba.service.GetRatesService;


@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetRatesServiceTest {
	
	@Autowired
	GetRatesService getRatesService;
	
	@Ignore
	@Test
	public void getRatesTest() {
		GetRateRequestDto requestDto = new GetRateRequestDto();
		requestDto.setSourceCountry("india");
		requestDto.setDestinationCountry("africa");
		
		GetRateResponseDto responseDto = getRatesService.getRates(requestDto);
		
		Assert.assertNotNull(responseDto);
	}

}
