package com.mfs.client.test.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetBankResponseDto;
import com.mfs.client.simba.service.GetBanksService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetBanksServiceTest {

	@Autowired
     GetBanksService getBanksService;
	
	@Ignore
	@Test
	public void bankTest()
	{
		GetBankRequestDto getBankRequestDto = new GetBankRequestDto();
		getBankRequestDto.setCountry("India");
		
		GetBankResponseDto getBankResponseDto = getBanksService.getBankDetails(getBankRequestDto);
		
		Assert.assertNotNull(getBankResponseDto);
	}
	
}