package com.mfs.client.simba.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dto.GetTransactionRequestDto;
import com.mfs.client.simba.dto.GetTransactionResponseDto;

import com.mfs.client.simba.service.GetTransactionService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetTransactionServiceTest {

	@Autowired
	GetTransactionService getTransactionService;

	@Ignore
	@Test
	public void bankTest() throws Exception {
		GetTransactionRequestDto getTransactionRequestDto = new GetTransactionRequestDto();
		getTransactionRequestDto.setTransactionReference("mmm");
		getTransactionRequestDto.setAgentReference("cccc");

		GetTransactionResponseDto getTransactionResponseDto = getTransactionService
				.getTransaction(getTransactionRequestDto);
		
		System.out.println(getTransactionResponseDto.toString());

		Assert.assertNotNull(getTransactionResponseDto);
	}

}
