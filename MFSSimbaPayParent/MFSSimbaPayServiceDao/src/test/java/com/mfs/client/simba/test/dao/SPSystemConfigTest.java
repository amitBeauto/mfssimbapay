package com.mfs.client.simba.test.dao;

import java.util.Map;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.exception.DaoException;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class SPSystemConfigTest {

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;
	
	private static final Logger LOGGER = Logger.getLogger(SPSystemConfigTest.class);
	
	@Test
	@Ignore
	public void getSystemConfigDetails()
	{
		Map<String, String> configMap=null;
		try {
			configMap = systemConfigDetailsDao.getConfigDetailsMap();
		} catch (DaoException e) {
			LOGGER.error(e);
			
		}
		Assert.assertNotNull(configMap);
	}

}
