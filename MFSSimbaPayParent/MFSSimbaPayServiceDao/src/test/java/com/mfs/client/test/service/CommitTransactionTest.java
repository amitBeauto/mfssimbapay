package com.mfs.client.test.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dto.Address;
import com.mfs.client.simba.dto.Bank;
import com.mfs.client.simba.dto.CommitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;
import com.mfs.client.simba.dto.Recipient;
import com.mfs.client.simba.dto.Sender;
import com.mfs.client.simba.dto.Transaction;
import com.mfs.client.simba.service.CommitTransactionService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class CommitTransactionTest {

	@Autowired
	CommitTransactionService commitTransactionService;

	@Ignore
	@Test
	public void commitTest() {
		InitTransactionRequestDto req = new InitTransactionRequestDto();
		CommitTransactionRequestDto commitReq = new CommitTransactionRequestDto();
		Sender sender = new Sender();
		Recipient recipient = new Recipient();
		Address address = new Address();
		Bank bank = new Bank();
		Transaction transaction = new Transaction();

		sender.setFirstName("Aakash");
		sender.setLastName("Dave");
		sender.setMobile("9898989898");
		sender.setDateOfBirth("1994-08-06");
		address.setAddressLine1("Suvoy Society");
		address.setCity("Pune");
		address.setPostcode("411052");
		address.setCountry("India");
		sender.setAddress(address);

		recipient.setFirstName("Pallavi");
		recipient.setLastName("Mhetre");
		recipient.setMobile("7878787878");
		bank.setAccountNumber("ACR234576");
		bank.setBankCode("BAN001");
		bank.setCountry("SA");
		recipient.setBankDetails(bank);

		transaction.setAgentReference("MFS001");
		transaction.setQuoteReference("SIM002");
		transaction.setCallBackUrl("/callbackurl");
		transaction.setSender(sender);
		transaction.setRecipient(recipient);

		req.setTransaction(transaction);
		commitReq.setQuoteReference("Quote101");
		InitTransactionResponseDto res = commitTransactionService.commitTransaction(commitReq, req);
		Assert.assertNotNull(res);
	}

}
