package com.mfs.client.test.service;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetQuoteResponseDto;
import com.mfs.client.simba.service.GetQuoteService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetQuoteServiceTest {

	@Autowired
	GetQuoteService getQuoteService;

	//@Ignore
	@Test
	public void getQuoteTest() {
		GetQuoteRequestDto requestDto = new GetQuoteRequestDto();
		requestDto.setAmount(1000);
		requestDto.setDeliveryMethod("ECO");
		requestDto.setDeliveryTime("EXP");
		requestDto.setDestinationCountry("SA");
		requestDto.setDestinationCurrency("frank");
		requestDto.setPaymentMethod("BANK");
		requestDto.setSourceCountry("IND");
		GetQuoteResponseDto responseDto = getQuoteService.getQuote(requestDto);
		
		Assert.assertNotNull(responseDto);
	}

}
