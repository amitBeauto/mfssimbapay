package com.mfs.client.simba.test.dao;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetStatusRequestDto;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPGetQuote;
import com.mfs.client.simba.model.SPGetRates;
import com.mfs.client.simba.model.SPTransactionLog;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class SPTransactionDaoTest {
	
	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(SPTransactionDaoTest.class);

	@Test
	@Ignore
	public void getTransationByTransId() {
		SPTransactionLog spTransactionLog = new SPTransactionLog();
		try {
			spTransactionLog = transactionDao.getTransationByTransId("MFS008");
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spTransactionLog);
	}
	
	@Test
	@Ignore
	public void getTransationLogByQuoteReference() {
		SPTransactionLog spTransactionLog = new SPTransactionLog();
		try {
			spTransactionLog = transactionDao.getTransationLogByQuoteReference("Quote101");
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spTransactionLog);
	}
	
	@Test
	@Ignore
	public void getTransationByTransRef() {
		SPTransactionLog spTransactionLog = new SPTransactionLog();
		try {
			spTransactionLog = transactionDao.getTransationByTransRef("TrRef101");
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spTransactionLog);
	}
	
	@Test
	@Ignore
	public void getTransactionByTransReference() {                         //failed
		SPTransactionLog spTransactionLog = new SPTransactionLog();
		try {
			GetStatusRequestDto req = new GetStatusRequestDto();
			req.setTransactionReference("TxnRef101");
			spTransactionLog = transactionDao.getTransactionByTransReference(req);
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spTransactionLog);
	}

	@Test
	@Ignore
	public void getQuoteObject() {
		SPGetQuote spGetQuote = new SPGetQuote();                             //falied
		try {
			GetQuoteRequestDto request = new GetQuoteRequestDto();
			request.setAmount(5000.0);
			request.setDeliveryMethod("MOB");
			request.setDeliveryTime("ECO");
			request.setDestinationCountry("AUS");
			request.setDestinationCurrency("KEN");
			request.setPaymentMethod("MOB");
			request.setSourceCountry("IND");
			spGetQuote = transactionDao.getQuoteObject(request);
			
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spGetQuote);
	}

	@Test
	@Ignore
	public void getSpBankObject1() {
		Object spBank = null;
		try {
			GetBankRequestDto request = new GetBankRequestDto();
			request.setCountry("IND");
			spBank = transactionDao.getSpBankObject1(request);
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spBank);
	}
	
	@Test
	@Ignore
	public void getSpRatesObject() {                      //Failed
		SPGetRates spRate = new SPGetRates();
		try {
			GetRateRequestDto request = new GetRateRequestDto();
			request.setSourceCountry("IND");
			request.setDestinationCountry("AUS");
			spRate = transactionDao.getSpRatesObject(request);
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spRate);
	}
	
	@Test
	@Ignore
	public void getTransationByQuoteReference() {                      
		SPGetQuote spQuote = new SPGetQuote();
		try {
			
			spQuote = transactionDao.getTransationByQuoteReference("SIM001");
		} catch (DaoException e) {
			LOGGER.error(e);
			System.out.println(e.getMessage());
		}

		Assert.assertNotNull(spQuote);
	}

}
