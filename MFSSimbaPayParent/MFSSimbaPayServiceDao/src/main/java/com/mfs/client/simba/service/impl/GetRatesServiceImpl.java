package com.mfs.client.simba.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.Delivery;
import com.mfs.client.simba.dto.Fee;
import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetRateResponseDto;
import com.mfs.client.simba.dto.Payment;
import com.mfs.client.simba.dto.Rates;
import com.mfs.client.simba.dto.ResponseStatus;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPDelivery;
import com.mfs.client.simba.model.SPFee;
import com.mfs.client.simba.model.SPGetRates;
import com.mfs.client.simba.model.SPPayment;
import com.mfs.client.simba.model.SPTime;
import com.mfs.client.simba.service.GetRatesService;
import com.mfs.client.simba.util.CallServices;
import com.mfs.client.simba.util.CommonConstant;
import com.mfs.client.simba.util.EncryptSHA256Util;
import com.mfs.client.simba.util.MFSSPCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("GetRatesService")
public class GetRatesServiceImpl implements GetRatesService {

	private static final Logger LOGGER = Logger.getLogger(GetRatesServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;

	public GetRateResponseDto getRates(GetRateRequestDto request) {
		LOGGER.info("==>In GetRatesServiceImpl in getRates function GetRateRequestDto =" + request);
		String jsonResponse;
		GetRateResponseDto getRateResponse = null;
		Gson gson = new Gson();
		Date date = new Date();
		long nonce = date.getTime();
		String signature = null;

		try {
			Map<String, String> systemConfig = systemConfigDetailsDao.getConfigDetailsMap();
			if (systemConfig == null) {
				throw new Exception("No Config Details in DB");
			}

			String companyId = systemConfig.get(CommonConstant.COMPANY_ID);
			if ((request.getSourceCountry() != null && request.getSourceCountry() != "")
					&& (request.getDestinationCountry() != null && request.getDestinationCountry() != "")) {

				signature = EncryptSHA256Util.EncryptSHA256Alogo(nonce, systemConfig.get(CommonConstant.COMPANY_ID),
						systemConfig.get(CommonConstant.SECRET));

				LOGGER.info("==> GetRatesServiceImpl -->getRates -->Signature: " + signature);

				LOGGER.info("==> getRates Request : " + gson.toJson(request));
				LOGGER.info("==> Sending Request to Url : " + systemConfig.get(CommonConstant.BASE_URL));

				jsonResponse = CallServices.getResponseFromService(
						(systemConfig.get(CommonConstant.BASE_URL) + systemConfig.get(CommonConstant.RATE_URL)),
						gson.toJson(request), signature, nonce, companyId);

				getRateResponse = (GetRateResponseDto) JSONToObjectConversion.getObjectFromJson(jsonResponse,
						GetRateResponseDto.class);
				LOGGER.info("jsonResponse: " + jsonResponse);
				LOGGER.info("getRateResponse: " + getRateResponse);

				logRateResponse(getRateResponse, request);

			} else {
				signature = EncryptSHA256Util.EncryptSHA256Alogo(nonce, systemConfig.get(CommonConstant.COMPANY_ID),
						systemConfig.get(CommonConstant.SECRET));

				LOGGER.info("==> GetRatesServiceImpl -->getRates -->Signature: " + signature);

				LOGGER.info("==> getRates Request : " + gson.toJson(request));
				LOGGER.info("==> Sending Request to Url : " + systemConfig.get(CommonConstant.BASE_URL));

				getRateResponse = new GetRateResponseDto();
				jsonResponse = CallServices.getResponseFromService(
						(systemConfig.get(CommonConstant.BASE_URL) + systemConfig.get(CommonConstant.RATE_URL)),
						gson.toJson(request), signature, nonce, companyId);

				getRateResponse = (GetRateResponseDto) JSONToObjectConversion.getObjectFromJson(jsonResponse,
						GetRateResponseDto.class);

				LOGGER.info("jsonResponse: " + jsonResponse);
				LOGGER.info("getRateResponse: " + getRateResponse);
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetRatesServiceImpl" + de);
			getRateResponse = new GetRateResponseDto();
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(de.getStatus().getStatusCode());
			status.setStatusMessage(de.getStatus().getStatusMessage());
			getRateResponse.setResponseStatus(status);

		} catch (Exception e) {
			LOGGER.error("==>Exception in GetRatesServiceImpl" + e);
			getRateResponse = new GetRateResponseDto();
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER203.getCode());
			status.setStatusMessage(MFSSPCode.ER203.getMessage());
			getRateResponse.setResponseStatus(status);
		}
		return getRateResponse;
	}

	private void logRateResponse(GetRateResponseDto getRateResponse, GetRateRequestDto request) throws DaoException {
		SPGetRates spGetRates;

		spGetRates = transactionDao.getSpRatesObject(request);
		if (spGetRates == null) {
			spGetRates = new SPGetRates();
			List<Fee> fees = null;

			SPDelivery delivery = new SPDelivery();
			List<SPDelivery> deliveryList = new ArrayList<SPDelivery>();
			SPPayment payment = null;
			List<SPPayment> paymentList = new ArrayList<SPPayment>();
			SPFee fee = null;
			SPTime time = new SPTime();
			List<SPTime> timeList = new ArrayList<SPTime>();

			List<Rates> rateList = getRateResponse.getRates();

			List<Delivery> delList = new ArrayList<Delivery>();
			List<Payment> payList = new ArrayList<Payment>();
			List<String> tList = new ArrayList<String>();

			for (Rates r : rateList) {

				delList = r.getDeliveries();
				payList = r.getPayments();
				tList = r.getTimes();

				for (Delivery d : delList) {
					delivery.setMethod(d.getMethod());
					delivery.setMinimum(d.getMinimum());
					delivery.setMaximum(d.getMaximum());
					delivery.setRate(d.getRate());
					delivery.setSpGetRates(spGetRates);
					deliveryList.add(delivery);

				}

				for (Payment p : payList) {
					fees = p.getFees();
					payment = new SPPayment();

					List<SPFee> feeList = new ArrayList<SPFee>();
					for (Fee f : fees) {
						fee = new SPFee();
						fee.setAmount(f.getAmount());
						fee.setLowerBound(f.getLowerBound());
						fee.setUpperBound(f.getUpperBound());
						fee.setDeliveryTime(f.getDeliveryTime());
						fee.setSpPayment(payment);

						feeList.add(fee);

					}
					payment.setMethod(p.getMethod());
					payment.setMinimum(p.getMinimum());
					payment.setMaximum(p.getMaximum());
					payment.setFees(feeList);
					payment.setSpGetRates(spGetRates);
					paymentList.add(payment);

				}

				for (String t : tList) {
					// time.setTimeCode(t.getTimeCode());
					time.setSpGetRates(spGetRates);
					timeList.add(time);

				}
				spGetRates.setDeliveries(deliveryList);
				spGetRates.setTimes(timeList);
				spGetRates.setPayments(paymentList);
				spGetRates.setSourceCurrency(r.getSourceCurrency());
				spGetRates.setDestinationCurrency(r.getDestinationCurrency());
				spGetRates.setSourceCountry(request.getSourceCountry());
				spGetRates.setDestinationCountry(request.getDestinationCountry());

				transactionDao.save(spGetRates);

			}
		}

	}
}
