package com.mfs.client.simba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sp_branch")
public class SPBranch {
	@Id
	@GeneratedValue
	@Column(name = "trans_branch_id")
	private long transBranchId;

	@Column(name = "branch_code")
	private String branchCode;

	@Column(name = "branch_name")
	private String branchName;

	@ManyToOne
	@JoinColumn(name = "sp_banks_id")
	private SPBank spBank;

	public long getTransBranchId() {
		return transBranchId;
	}

	public void setTransBranchId(long transBranchId) {
		this.transBranchId = transBranchId;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public SPBank getSpBank() {
		return spBank;
	}

	public void setSpBank(SPBank spBank) {
		this.spBank = spBank;
	}

	@Override
	public String toString() {
		return "SPBranch [transBranchId=" + transBranchId + ", branchCode=" + branchCode + ", branchName=" + branchName
				+ "]";
	}
	

}
