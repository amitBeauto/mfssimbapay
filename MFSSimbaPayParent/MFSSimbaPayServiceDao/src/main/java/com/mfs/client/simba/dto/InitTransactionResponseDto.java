package com.mfs.client.simba.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InitTransactionResponseDto {
	
	private String quoteReference;
	private String transactionReference;
	private String status; 
	private String message;
	private String code;
	
	public String getTransactionReference() {
		return transactionReference;
	}
	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getQuoteReference() {
		return quoteReference;
	}
	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "InitTransactionResponseDto [quoteReference=" + quoteReference + ", transactionReference="
				+ transactionReference + ", status=" + status + ", message=" + message + ", code=" + code + "]";
	}

}
