package com.mfs.client.simba.dto;

public class GetStatusRequestDto {

	private String transactionReference;
	private String agentReference;
	private String status;
	private String message;
	
	public String getTransactionReference() {
		return transactionReference;
	}
	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}
	public String getAgentReference() {
		return agentReference;
	}
	public void setAgentReference(String agentReference) {
		this.agentReference = agentReference;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GetStatusRequestDto [transactionReference=" + transactionReference + ", agentReference="
				+ agentReference + ", status=" + status + ", message=" + message + "]";
	}
	
}
