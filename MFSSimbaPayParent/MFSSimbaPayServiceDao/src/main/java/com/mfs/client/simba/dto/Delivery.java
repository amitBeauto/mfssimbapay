package com.mfs.client.simba.dto;

import java.util.List;

public class Delivery {

	private String method;
	private double minimum;
	private double maximum;
	private List<Double> amounts;
	private double rate;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public double getMinimum() {
		return minimum;
	}

	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}

	public double getMaximum() {
		return maximum;
	}

	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}

	public List<Double> getAmounts() {
		return amounts;
	}

	public void setAmounts(List<Double> amounts) {
		this.amounts = amounts;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "Delivery [method=" + method + ", minimum=" + minimum + ", maximum=" + maximum + ", amounts=" + amounts
				+ ", rate=" + rate + "]";
	}

}
