package com.mfs.client.simba.service;

import java.util.Date;

public interface BaseService {
	
	public void logTransSession(String quoteReference, String mfsTransId, String transactionReference, String callbackUrl,String senderFirstName,
			String senderLastName, String senderMobile, String senderPostcode, String senderCountry, String recipientFirstName,
			String recipientLastName, String recipientMobile, String recipientCountry, String accountNumber, String bankCode,
			String status, String message, Date dateLogged, String type, String number, String issuedBy, String startDate,
			String expiryDate);

}
