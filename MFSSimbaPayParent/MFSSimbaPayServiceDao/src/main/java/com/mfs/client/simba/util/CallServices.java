package com.mfs.client.simba.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mfs.client.util.HttpConnectorImpl;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.HttpsConnectorImpl;


public class CallServices {
	
private static final Logger LOGGER = Logger.getLogger(CallServices.class);
	
	public static String getResponseFromService(String url, String request,String signature, long nonce,String companyId) throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;
		
		LOGGER.info("In CallServices in getResponseFromService(url,request,signature Request Body =>");
		LOGGER.info("url="+url+ " " +"request= "+request +" "+"signature="+ signature);
		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			
			Map<String, String> headerMap = new HashMap<String, String>();

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			headerMap.put(CommonConstant.SIGNATURE, signature);
			headerMap.put(CommonConstant.NONCE, String.valueOf(nonce));
			headerMap.put(CommonConstant.COMPANYID, companyId);
			
			LOGGER.debug("URL : "+url);
			connectionRequest.setServiceUrl(url);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");
			
			/*HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,request);
			*/
			boolean isHttps = true;
			if (isHttps) {
				connectionRequest.setPort(8443);
				HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			} else {
				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,request);
			}
			str_result=httpsConResult.getResponseData();
			
			LOGGER.info("In CallServices in getResponseFromService(url,request,signature) Response Body =>");
			LOGGER.info(str_result);
		} catch (Exception e) {			
			LOGGER.error("==>Exception thrown in CallServices in getResponseFromService(url,request,signature) : "+e);
			throw new Exception(e);
		}
		return str_result;
	}
	
	
	public static String getResponseFromAuthService(String url, String request) throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;
			
		LOGGER.info("In Callservices in getResponseFromAuthService Request Body =>");
		LOGGER.info("url= "+url+" "+"request= "+request);
		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			
			Map<String, String> headerMap = new HashMap<String, String>();
			
		//	headerMap.put(CommonConstant.Cache_Control,CommonConstant.no_cache );
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);

			LOGGER.debug("URL : "+url);
			
			connectionRequest.setServiceUrl(url);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");
			
			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,request);
			
			/*boolean isHttps = true;
			if (isHttps) {
				
				System.out.println("in if");
				connectionRequest.setPort(8443);
				HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);
			} else {
				System.out.println("in else");
				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,request);
			}
			  */
			str_result=httpsConResult.getResponseData();
			
			LOGGER.info("In Callservices in getResponseFromAuthService Response Body =>");
			LOGGER.info(str_result);
		} catch (Exception e) {			
			LOGGER.error("==>Exception thrown in CallServices in getResponseFromAuthService : "+e);
			throw new Exception(e);
		}
		return str_result;
	}
	
	
	public static String getResponseFromService(String url,String signature) throws Exception {

		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;
		
		LOGGER.info("In Callservices in getResponseFromService(url,signature) Request Body =>");
		LOGGER.info("url= "+url +" "+"signature= "+ signature);
		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			
			Map<String, String> headerMap = new HashMap<String, String>();

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			headerMap.put(CommonConstant.AUTHORIZATION, signature);

			LOGGER.debug("URL : "+url);
			connectionRequest.setServiceUrl(url);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("POST");
			
			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,signature);
			str_result=httpsConResult.getResponseData();
			
			LOGGER.info("In Callservices in getResponseFromService(url,accessToken) Response Body =>");
			LOGGER.info(str_result);
		} catch (Exception e) {			
			LOGGER.error("==>Exception thrown in CallServices in getResponseFromService(url,accessToken) : "+e);
			throw new Exception(e);
		}
		return str_result;
	}
}
