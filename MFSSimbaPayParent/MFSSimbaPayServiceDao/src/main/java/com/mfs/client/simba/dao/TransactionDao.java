package com.mfs.client.simba.dao;

import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetStatusRequestDto;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPGetQuote;
import com.mfs.client.simba.model.SPGetRates;
import com.mfs.client.simba.model.SPTransactionLog;

public interface TransactionDao extends BaseDao {

	public SPGetQuote getQuoteObject(GetQuoteRequestDto request)throws DaoException ;

	public SPGetRates getSpRatesObject(GetRateRequestDto request) throws DaoException;
	
	public Object getSpBankObject1(GetBankRequestDto request) throws DaoException;
	
	public Object getSpBankObject(GetBankRequestDto request) throws DaoException;
	
	public SPTransactionLog getTransationByTransId(String mfsTransId) throws DaoException;
	
	public SPGetQuote getTransationByQuoteReference(String quoteReference) throws DaoException;

	public SPTransactionLog getTransationLogByQuoteReference(String quoteReference) throws DaoException;
	
	public SPTransactionLog getTransactionByTransReference(GetStatusRequestDto request) throws DaoException;
	
	SPTransactionLog getTransationByTransRef(String mfsTransId) throws DaoException;

	public Object getALLSpRatesObject()throws DaoException;
}
