package com.mfs.client.simba.dao;

import java.util.Map;

import com.mfs.client.simba.exception.DaoException;

public interface SystemConfigDetailsDao {
	
	public Map<String,String> getConfigDetailsMap()throws DaoException;

}
