package com.mfs.client.simba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sp_delivery")
public class SPDelivery {

	@Id
	@GeneratedValue
	@Column(name = "trans_delivery_id")
	private long transDeliveryId;

	@Column(name = "method")
	private String method;

	@Column(name = "minimum")
	private double minimum;

	@Column(name = "maximum")
	private double maximum;

	@Column(name = "amounts")
	private double amounts;

	@Column(name = "rate")
	private double rate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="sp_get_rate_id")
	private SPGetRates spGetRates;

	public long getTransDeliveryId() {
		return transDeliveryId;
	}

	public void setTransDeliveryId(long transDeliveryId) {
		this.transDeliveryId = transDeliveryId;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public double getMinimum() {
		return minimum;
	}

	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}

	public double getMaximum() {
		return maximum;
	}

	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}

	public double getAmounts() {
		return amounts;
	}

	public void setAmounts(double amounts) {
		this.amounts = amounts;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public SPGetRates getSpGetRates() {
		return spGetRates;
	}

	public void setSpGetRates(SPGetRates spGetRates) {
		this.spGetRates = spGetRates;
	}

	@Override
	public String toString() {
		return "SPDelivery [transDeliveryId=" + transDeliveryId + ", method=" + method + ", minimum=" + minimum
				+ ", maximum=" + maximum + ", amounts=" + amounts + ", rate=" + rate + ", spGetRates=" + spGetRates
				+ "]";
	}
	
	
}
