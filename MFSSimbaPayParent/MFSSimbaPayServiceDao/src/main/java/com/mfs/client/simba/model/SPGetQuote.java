package com.mfs.client.simba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sp_get_quote")
public class SPGetQuote {

	@Id
	@GeneratedValue
	@Column(name = "trans_quote_id")
	private long transQuoteId;

	@Column(name = "source_country")
	private String sourceCountry;

	@Column(name = "destination_country")
	private String destinationCountry;

	@Column(name = "destination_currency")
	private String destinationCurrency;

	@Column(name = "delivery_method")
	private String deliveryMethod;

	@Column(name = "delivery_time")
	private String deliveryTime;

	@Column(name = "payment_method")
	private String paymentMethod;

	@Column(name = "amount")
	private double amount;

	@Column(name = "rate")
	private double rate;

	@Column(name = "fee")
	private double fee;

	@Column(name = "destination_amount")
	private double destinationAmount;

	@Column(name = "source_amount")
	private double sourceAmount;

	@Column(name = "tax")
	private double tax;

	@Column(name = "amount_to_pay")
	private double amountToPay;

	@Column(name = "quote_reference",unique = true)
	private String quoteReference;

	@Column(name = "message")
	private String message;
	
	public SPGetQuote() {
		
	}

	public long getTransQuoteId() {
		return transQuoteId;
	}

	public void setTransQuoteId(long transQuoteId) {
		this.transQuoteId = transQuoteId;
	}

	public String getSourceCountry() {
		return sourceCountry;
	}

	public void setSourceCountry(String sourceCountry) {
		this.sourceCountry = sourceCountry;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getDestinationCurrency() {
		return destinationCurrency;
	}

	public void setDestinationCurrency(String destinationCurrency) {
		this.destinationCurrency = destinationCurrency;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public double getDestinationAmount() {
		return destinationAmount;
	}

	public void setDestinationAmount(double destinationAmount) {
		this.destinationAmount = destinationAmount;
	}

	public double getSourceAmount() {
		return sourceAmount;
	}

	public void setSourceAmount(double sourceAmount) {
		this.sourceAmount = sourceAmount;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getAmountToPay() {
		return amountToPay;
	}

	public void setAmountToPay(double amountToPay) {
		this.amountToPay = amountToPay;
	}

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "SPGetQuote [transQuoteId=" + transQuoteId + ", sourceCountry=" + sourceCountry + ", destinationCountry="
				+ destinationCountry + ", destinationCurrency=" + destinationCurrency + ", deliveryMethod="
				+ deliveryMethod + ", deliveryTime=" + deliveryTime + ", paymentMethod=" + paymentMethod + ", amount="
				+ amount + ", rate=" + rate + ", fee=" + fee + ", destinationAmount=" + destinationAmount
				+ ", sourceAmount=" + sourceAmount + ", tax=" + tax + ", amountToPay=" + amountToPay
				+ ", quoteReference=" + quoteReference + ", message=" + message + "]";
	}

	
}
