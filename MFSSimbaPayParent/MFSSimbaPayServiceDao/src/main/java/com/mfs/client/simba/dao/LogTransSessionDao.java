package com.mfs.client.simba.dao;

import com.mfs.client.simba.model.SPTransSession;

public interface LogTransSessionDao {
	public long logTransSession(SPTransSession spTransSession);
}
