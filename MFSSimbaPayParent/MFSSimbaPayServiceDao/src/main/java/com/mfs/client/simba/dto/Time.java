package com.mfs.client.simba.dto;

public class Time {

	private String timeCode;

	public String getTimeCode() {
		return timeCode;
	}

	public void setTimeCode(String timeCode) {
		this.timeCode = timeCode;
	}

	@Override
	public String toString() {
		return "Time [timeCode=" + timeCode + "]";
	}
	
	
}
