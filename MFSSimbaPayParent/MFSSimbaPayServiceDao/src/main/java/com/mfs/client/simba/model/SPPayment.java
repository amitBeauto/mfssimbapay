package com.mfs.client.simba.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sp_payment")
public class SPPayment {

	@Id
	@GeneratedValue
	@Column(name = "trans_payment_id")
	private long transPaymentId;

	@Column(name = "method")
	private String method;

	@Column(name = "minimum")
	private double minimum;

	@Column(name = "maximum")
	private double maximum;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sp_get_rate_id")
	private SPGetRates spGetRates;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "spPayment")
	private List<SPFee> fees;

	public long getTransPaymentId() {
		return transPaymentId;
	}

	public void setTransPaymentId(long transPaymentId) {
		this.transPaymentId = transPaymentId;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public double getMinimum() {
		return minimum;
	}

	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}

	public double getMaximum() {
		return maximum;
	}

	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}

	public SPGetRates getSpGetRates() {
		return spGetRates;
	}

	public void setSpGetRates(SPGetRates spGetRates) {
		this.spGetRates = spGetRates;
	}

	public List<SPFee> getFees() {
		return fees;
	}

	public void setFees(List<SPFee> fees) {
		this.fees = fees;
	}

	@Override
	public String toString() {
		return "SPPayment [transPaymentId=" + transPaymentId + ", method=" + method + ", minimum=" + minimum
				+ ", maximum=" + maximum + ", spGetRates=" + spGetRates + ", fees=" + fees + "]";
	}

}
