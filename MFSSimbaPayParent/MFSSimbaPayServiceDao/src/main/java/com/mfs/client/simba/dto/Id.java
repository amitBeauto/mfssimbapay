package com.mfs.client.simba.dto;

public class Id {
	
	private String type;
	private String number;
	private String issuedBy;
	private String startDate;
	private String expiryDate;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "Id [type=" + type + ", number=" + number + ", issuedBy=" + issuedBy + ", startDate=" + startDate
				+ ", expiryDate=" + expiryDate + "]";
	}

}
