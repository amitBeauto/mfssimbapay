package com.mfs.client.simba.dto;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetBankResponseDto {

	private List<Banks> banks;

	private ResponseStatus responseStatus;

	public List<Banks> getBanks() {
		return banks;
	}

	public void setBanks(List<Banks> banks) {
		this.banks = banks;
	}

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public String toString() {
		return "GetBankResponseDto [banks=" + banks + ", responseStatus=" + responseStatus + "]";
	}
}
