package com.mfs.client.simba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sp_system_config")
public class SPSystemConfig {
	@Id
	@GeneratedValue
	@Column(name = "system_config_id")
	private long systemConfigId;

	@Column(name = "config_key")
	private String configKey;

	@Column(name = "config_value")
	private String configValue;

	public long getSystemConfigId() {
		return systemConfigId;
	}

	public void setSystemConfigId(long systemConfigId) {
		this.systemConfigId = systemConfigId;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}

	@Override
	public String toString() {
		return "SPSystemConfig [systemConfigId=" + systemConfigId + ", configKey=" + configKey + ", configValue="
				+ configValue + "]";
	}

	
}
