package com.mfs.client.simba.dto;

public class Fee {

	private double amount;
	private double lowerBound;
	private double upperBound;
	private String deliveryTime;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}

	public double getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	@Override
	public String toString() {
		return "Fee [amount=" + amount + ", lowerBound=" + lowerBound + ", upperBound=" + upperBound + ", deliveryTime="
				+ deliveryTime + "]";
	}

}
