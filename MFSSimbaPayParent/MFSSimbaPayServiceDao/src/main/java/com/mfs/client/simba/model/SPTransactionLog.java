package com.mfs.client.simba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sp_transaction_log")
public class SPTransactionLog {
	@Id
	@GeneratedValue
	@Column(name = "transaction_id")
	private long transactionId;

	@Column(name = "quote_reference")
	private String quoteReference;

	@Column(name = "transaction_reference")
	private String transactionReference;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "call_back_url")
	private String callBackUrl;

	@Column(name = "sender_first_name")
	private String senderFirstName;

	@Column(name = "sender_last_name")
	private String senderLastName;

	@Column(name = "sender_mobile")
	private String senderMobile;

	@Column(name = "sender_address_line1")
	private String senderAddressLine1;

	@Column(name = "sender_address_line2")
	private String senderAddressLine2;

	@Column(name = "sender_date_of_birth")
	private String senderDateOfBirth;

	@Column(name = "sender_email")
	private String senderEmail;

	@Column(name = "sender_nationality")
	private String senderNationality;

	@Column(name = "sender_city")
	private String senderCity;

	@Column(name = "sender_state")
	private String sendertState;

	@Column(name = "sender_postcode")
	private String senderPostcode;

	@Column(name = "sender_country")
	private String senderCountry;

	@Column(name = "recipient_first_name")
	private String recipientFirstName;

	@Column(name = "recipient_last_name")
	private String recipientLastName;

	@Column(name = "recipient_mobile")
	private String recipientMobile;

	@Column(name = "recipient_address_line1")
	private String recipientAddressLine1;

	@Column(name = "recipient_address_line2")
	private String recipientAddressLine2;

	@Column(name = "recipient_email")
	private String recipientEmail;

	@Column(name = "recipient_city")
	private String recipientCity;

	@Column(name = "recipient_state")
	private String recipientState;

	@Column(name = "recipient_postcode")
	private String recipientPostcode;

	@Column(name = "recipient_country")
	private String recipientCountry;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "bank_code")
	private String bankCode;

	@Column(name = "branch_code")
	private String branchCode;

	@Column(name = "status")
	private String status;

	@Column(name = "message")
	private String message;

	@Column(name = "type")
	private String type;

	@Column(name = "number")
	private String number;

	@Column(name = "issued_by")
	private String issuedBy;

	@Column(name = "start_date")
	private String startDate;

	@Column(name = "expiry_date")
	private String expiryDate;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getSenderMobile() {
		return senderMobile;
	}

	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}

	public String getSenderAddressLine1() {
		return senderAddressLine1;
	}

	public void setSenderAddressLine1(String senderAddressLine1) {
		this.senderAddressLine1 = senderAddressLine1;
	}

	public String getSenderAddressLine2() {
		return senderAddressLine2;
	}

	public void setSenderAddressLine2(String senderAddressLine2) {
		this.senderAddressLine2 = senderAddressLine2;
	}

	public String getSenderDateOfBirth() {
		return senderDateOfBirth;
	}

	public void setSenderDateOfBirth(String senderDateOfBirth) {
		this.senderDateOfBirth = senderDateOfBirth;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSenderNationality() {
		return senderNationality;
	}

	public void setSenderNationality(String senderNationality) {
		this.senderNationality = senderNationality;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getSendertState() {
		return sendertState;
	}

	public void setSendertState(String sendertState) {
		this.sendertState = sendertState;
	}

	public String getSenderPostcode() {
		return senderPostcode;
	}

	public void setSenderPostcode(String senderPostcode) {
		this.senderPostcode = senderPostcode;
	}

	public String getSenderCountry() {
		return senderCountry;
	}

	public void setSenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}

	public String getRecipientFirstName() {
		return recipientFirstName;
	}

	public void setRecipientFirstName(String recipientFirstName) {
		this.recipientFirstName = recipientFirstName;
	}

	public String getRecipientLastName() {
		return recipientLastName;
	}

	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}

	public String getRecipientMobile() {
		return recipientMobile;
	}

	public void setRecipientMobile(String recipientMobile) {
		this.recipientMobile = recipientMobile;
	}

	public String getRecipientAddressLine1() {
		return recipientAddressLine1;
	}

	public void setRecipientAddressLine1(String recipientAddressLine1) {
		this.recipientAddressLine1 = recipientAddressLine1;
	}

	public String getRecipientAddressLine2() {
		return recipientAddressLine2;
	}

	public void setRecipientAddressLine2(String recipientAddressLine2) {
		this.recipientAddressLine2 = recipientAddressLine2;
	}

	public String getRecipientEmail() {
		return recipientEmail;
	}

	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}

	public String getRecipientCity() {
		return recipientCity;
	}

	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}

	public String getRecipientState() {
		return recipientState;
	}

	public void setRecipientState(String recipientState) {
		this.recipientState = recipientState;
	}

	public String getRecipientPostcode() {
		return recipientPostcode;
	}

	public void setRecipientPostcode(String recipientPostcode) {
		this.recipientPostcode = recipientPostcode;
	}

	public String getRecipientCountry() {
		return recipientCountry;
	}

	public void setRecipientCountry(String recipientCountry) {
		this.recipientCountry = recipientCountry;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "SPTransactionLog [transactionId=" + transactionId + ", quoteReference=" + quoteReference
				+ ", transactionReference=" + transactionReference + ", mfsTransId=" + mfsTransId + ", callBackUrl="
				+ callBackUrl + ", senderFirstName=" + senderFirstName + ", senderLastName=" + senderLastName
				+ ", senderMobile=" + senderMobile + ", senderAddressLine1=" + senderAddressLine1
				+ ", senderAddressLine2=" + senderAddressLine2 + ", senderDateOfBirth=" + senderDateOfBirth
				+ ", senderEmail=" + senderEmail + ", senderNationality=" + senderNationality + ", senderCity="
				+ senderCity + ", sendertState=" + sendertState + ", senderPostcode=" + senderPostcode
				+ ", senderCountry=" + senderCountry + ", recipientFirstName=" + recipientFirstName
				+ ", recipientLastName=" + recipientLastName + ", recipientMobile=" + recipientMobile
				+ ", recipientAddressLine1=" + recipientAddressLine1 + ", recipientAddressLine2="
				+ recipientAddressLine2 + ", recipientEmail=" + recipientEmail + ", recipientCity=" + recipientCity
				+ ", recipientState=" + recipientState + ", recipientPostcode=" + recipientPostcode
				+ ", recipientCountry=" + recipientCountry + ", accountNumber=" + accountNumber + ", bankCode="
				+ bankCode + ", branchCode=" + branchCode + ", status=" + status + ", message=" + message + ", type="
				+ type + ", number=" + number + ", issuedBy=" + issuedBy + ", startDate=" + startDate + ", expiryDate="
				+ expiryDate + "]";
	}

}
