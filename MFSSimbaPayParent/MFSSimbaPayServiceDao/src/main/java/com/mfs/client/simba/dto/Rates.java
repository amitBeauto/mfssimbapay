package com.mfs.client.simba.dto;

import java.util.List;

public class Rates {
	
	private String sourceCurrency;
	private String destinationCurrency;
	private String destinationCountry;
	private List<Delivery> deliveries;
	private List<Payment> payments;
	private List<String> times;
	public String getSourceCurrency() {
		return sourceCurrency;
	}
	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}
	public String getDestinationCurrency() {
		return destinationCurrency;
	}
	public void setDestinationCurrency(String destinationCurrency) {
		this.destinationCurrency = destinationCurrency;
	}
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	public List<Delivery> getDeliveries() {
		return deliveries;
	}
	public void setDeliveries(List<Delivery> deliveries) {
		this.deliveries = deliveries;
	}
	public List<Payment> getPayments() {
		return payments;
	}
	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}
	public List<String> getTimes() {
		return times;
	}
	public void setTimes(List<String> times) {
		this.times = times;
	}
	@Override
	public String toString() {
		return "Rates [sourceCurrency=" + sourceCurrency + ", destinationCurrency=" + destinationCurrency
				+ ", destinationCountry=" + destinationCountry + "]";
	}
	
	
	
}
