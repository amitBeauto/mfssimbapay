package com.mfs.client.simba.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sp_trans_session")
public class SPTransSession {
	@Id
	@GeneratedValue
	@Column(name = "trans_session_id")
	private long trans_session_id;

	@Column(name = "quote_reference")
	private String quoteReference;

	@Column(name = "transaction_reference")
	private String transactionReference;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "call_back_url")
	private String callBackUrl;

	@Column(name = "sender_first_name")
	private String senderFirstName;

	@Column(name = "sender_last_name")
	private String senderLastName;

	@Column(name = "sender_mobile")
	private String senderMobile;

	@Column(name = "sender_postcode")
	private String senderPostcode;

	@Column(name = "sender_country")
	private String senderCountry;

	@Column(name = "recipient_first_name")
	private String recipientFirstName;

	@Column(name = "recipient_last_name")
	private String recipientLastName;

	@Column(name = "recipient_mobile")
	private String recipientMobile;

	@Column(name = "recipient_country")
	private String recipientCountry;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "bank_code")
	private String bankCode;

	@Column(name = "status")
	private String status;

	@Column(name = "date_logged")
	private Date date;

	@Column(name = "message")
	private String message;

	@Column(name = "type")
	private String type;

	@Column(name = "number")
	private String number;

	@Column(name = "issued_by")
	private String issuedBy;

	@Column(name = "start_date")
	private String startDate;

	@Column(name = "expiry_date")
	private String expiryDate;

	public String getTransactionReference() {
		return transactionReference;
	}

	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getTrans_session_id() {
		return trans_session_id;
	}

	public void setTrans_session_id(long trans_session_id) {
		this.trans_session_id = trans_session_id;
	}

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getSenderMobile() {
		return senderMobile;
	}

	public void setSenderMobile(String senderMobile) {
		this.senderMobile = senderMobile;
	}

	public String getSenderPostcode() {
		return senderPostcode;
	}

	public void setSenderPostcode(String senderPostcode) {
		this.senderPostcode = senderPostcode;
	}

	public String getSenderCountry() {
		return senderCountry;
	}

	public void setSenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}

	public String getRecipientFirstName() {
		return recipientFirstName;
	}

	public void setRecipientFirstName(String recipientFirstName) {
		this.recipientFirstName = recipientFirstName;
	}

	public String getRecipientLastName() {
		return recipientLastName;
	}

	public void setRecipientLastName(String recipientLastName) {
		this.recipientLastName = recipientLastName;
	}

	public String getRecipientMobile() {
		return recipientMobile;
	}

	public void setRecipientMobile(String recipientMobile) {
		this.recipientMobile = recipientMobile;
	}

	public String getRecipientCountry() {
		return recipientCountry;
	}

	public void setRecipientCountry(String recipientCountry) {
		this.recipientCountry = recipientCountry;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	@Override
	public String toString() {
		return "SPTransSession [trans_session_id=" + trans_session_id + ", quoteReference=" + quoteReference
				+ ", transactionReference=" + transactionReference + ", mfsTransId=" + mfsTransId + ", callBackUrl="
				+ callBackUrl + ", senderFirstName=" + senderFirstName + ", senderLastName=" + senderLastName
				+ ", senderMobile=" + senderMobile + ", senderPostcode=" + senderPostcode + ", senderCountry="
				+ senderCountry + ", recipientFirstName=" + recipientFirstName + ", recipientLastName="
				+ recipientLastName + ", recipientMobile=" + recipientMobile + ", recipientCountry=" + recipientCountry
				+ ", accountNumber=" + accountNumber + ", bankCode=" + bankCode + ", status=" + status + ", date="
				+ date + ", message=" + message + ", type=" + type + ", number=" + number + ", issuedBy=" + issuedBy
				+ ", startDate=" + startDate + ", expiryDate=" + expiryDate + "]";
	}

}
