package com.mfs.client.simba.dto;

import java.util.List;

public class Payment {

	private String method;
	private double minimum;
	private double maximum;
	private List<Fee> fees;
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public double getMinimum() {
		return minimum;
	}
	public void setMinimum(double minimum) {
		this.minimum = minimum;
	}
	public double getMaximum() {
		return maximum;
	}
	public void setMaximum(double maximum) {
		this.maximum = maximum;
	}
	public List<Fee> getFees() {
		return fees;
	}
	public void setFees(List<Fee> fees) {
		this.fees = fees;
	}
	@Override
	public String toString() {
		return "Payment [method=" + method + ", minimum=" + minimum + ", maximum=" + maximum + ", fees=" + fees + "]";
	}
	
	
}
