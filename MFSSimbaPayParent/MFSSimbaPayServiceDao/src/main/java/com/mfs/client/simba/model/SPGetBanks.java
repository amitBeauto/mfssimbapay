package com.mfs.client.simba.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sp_get_banks")
public class SPGetBanks {
	@Id
	@GeneratedValue
	@Column(name = "trans_get_bank_id")
	private long transBankId;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "spGetBanks")
	private List<SPBank> banks;

	public long getTransBankId() {
		return transBankId;
	}

	public void setTransBankId(long transBankId) {
		this.transBankId = transBankId;
	}

	public List<SPBank> getBanks() {
		return banks;
	}

	public void setBanks(List<SPBank> banks) {
		this.banks = banks;
	}

	@Override
	public String toString() {
		return "SPGetBanks [transBankId=" + transBankId + ", banks=" + banks + "]";
	}

}
