package com.mfs.client.simba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sp_fee")
public class SPFee {

	@Id
	@GeneratedValue
	@Column(name = "trans_fee_id")
	private long transFeeId;

	@Column(name = "amount")
	private double amount;

	@Column(name = "lower_bound")
	private double lowerBound;

	@Column(name = "upper_bound")
	private double upperBound;

	@Column(name = "delivery_time")
	private String deliveryTime;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "payment_id")
	private SPPayment spPayment;

	public long getTransFeeId() {
		return transFeeId;
	}

	public void setTransFeeId(long transFeeId) {
		this.transFeeId = transFeeId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}

	public double getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public SPPayment getSpPayment() {
		return spPayment;
	}

	public void setSpPayment(SPPayment spPayment) {
		this.spPayment = spPayment;
	}

	@Override
	public String toString() {
		return "SPFee [transFeeId=" + transFeeId + ", amount=" + amount + ", lowerBound=" + lowerBound + ", upperBound="
				+ upperBound + ", deliveryTime=" + deliveryTime + ", spPayment=" + spPayment + "]";
	}

}
