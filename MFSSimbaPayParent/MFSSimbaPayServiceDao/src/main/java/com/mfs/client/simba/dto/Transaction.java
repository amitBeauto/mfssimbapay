package com.mfs.client.simba.dto;

public class Transaction {

	private Sender sender;
	private Recipient recipient;
	private String quoteReference;
	private String agentReference;
	private String callBackUrl;

	public Sender getSender() {
		return sender;
	}

	public void setSender(Sender sender) {
		this.sender = sender;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	public String getAgentReference() {
		return agentReference;
	}

	public void setAgentReference(String agentReference) {
		this.agentReference = agentReference;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	@Override
	public String toString() {
		return "Transaction [sender=" + sender + ", recipient=" + recipient + ", quoteReference=" + quoteReference
				+ ", agentReference=" + agentReference + ", callBackUrl=" + callBackUrl + "]";
	}

}
