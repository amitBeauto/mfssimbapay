package com.mfs.client.simba.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.Banks;
import com.mfs.client.simba.dto.Branch;
import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetBankResponseDto;
import com.mfs.client.simba.dto.ResponseStatus;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPBank;
import com.mfs.client.simba.model.SPBranch;
import com.mfs.client.simba.service.GetBanksService;
import com.mfs.client.simba.util.CallServices;
import com.mfs.client.simba.util.CommonConstant;
import com.mfs.client.simba.util.EncryptSHA256Util;
import com.mfs.client.simba.util.MFSSPCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("GetBanksService")

public class GetBanksServiceImpl implements GetBanksService {

	private static final Logger LOGGER = Logger.getLogger(GetBanksServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;

	public GetBankResponseDto getBankDetails(GetBankRequestDto request) {

		LOGGER.info("==>In GetBanksServiceImpl in GetBank function GetBankRequest =" + request);

		String serviceResponse;
		Gson gson = new Gson();

		GetBankResponseDto response = null;
		Date date = new Date();
		long nonce = date.getTime();
		String signature = "";

		try {

			Map<String, String> systemConfig = systemConfigDetailsDao.getConfigDetailsMap();
			if (systemConfig == null) {
				throw new Exception("No Config Details in DB");
			}
			String companyId = systemConfig.get(CommonConstant.COMPANY_ID);
			signature = EncryptSHA256Util.EncryptSHA256Alogo(nonce, systemConfig.get(CommonConstant.COMPANY_ID),
					systemConfig.get(CommonConstant.SECRET));

			LOGGER.info("==> getbank Request : " + gson.toJson(request));
			LOGGER.info("==> Sending Request to Url : " + systemConfig.get(CommonConstant.BASE_URL));

			serviceResponse = CallServices.getResponseFromService(
					(systemConfig.get(CommonConstant.BASE_URL) + systemConfig.get(CommonConstant.BANK_URL)),
					gson.toJson(request), signature, nonce, companyId);

			response = (GetBankResponseDto) JSONToObjectConversion.getObjectFromJson(serviceResponse,
					GetBankResponseDto.class);

			if (response.getBanks() != null) {
				if (request.getCountry() != "") {
					LOGGER.info("serviceResponse: " + serviceResponse);
					LOGGER.info("response: " + response);
					logGetBanks(response, request);
				} else if (request.getCountry() == "") {
					LOGGER.info("serviceResponse: " + serviceResponse);
					LOGGER.info("response: " + response);
					logAllBanks(response, request);
				}

			} else {

				response = new GetBankResponseDto();
				ResponseStatus status = new ResponseStatus();
				status.setStatusCode(MFSSPCode.ER206.getCode());
				status.setStatusMessage(MFSSPCode.ER206.getMessage());
				response.setResponseStatus(status);

			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetBanksServiceImpl" + de);
			response = new GetBankResponseDto();
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(de.getStatus().getStatusCode());
			status.setStatusMessage(de.getStatus().getStatusMessage());
			response.setResponseStatus(status);

		} catch (Exception e) {

			LOGGER.error("Exception in GetBanksServiceImpl " + e);

			response = new GetBankResponseDto();
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER203.getCode());
			status.setStatusMessage(MFSSPCode.ER203.getMessage());
			response.setResponseStatus(status);

		}

		return response;
	}

	private void logAllBanks(GetBankResponseDto response, GetBankRequestDto request) throws DaoException {

		List<SPBank> sBankList = (List<SPBank>) transactionDao.getSpBankObject1(request);

		List<Banks> responseBankList = response.getBanks();

		List<Branch> branchlist = new ArrayList<Branch>();

		SPBranch spBranch = null;
		SPBank spBank = null;

		if (sBankList.size() == 0) {
			for (Banks b : responseBankList) {
				spBank = new SPBank();
				spBank.setBankCode(b.getCode());
				spBank.setBankCountry(b.getCountry());
				spBank.setBankName(b.getName());
				transactionDao.save(spBank);

				branchlist = b.getBranches();
				if (branchlist != null) {
					for (Branch b2 : branchlist) {
						spBranch = new SPBranch();
						spBranch.setBranchCode(b2.getCode());
						spBranch.setBranchName(b2.getName());
						spBranch.setSpBank(spBank);
						transactionDao.save(spBranch);

					}
				}
			}
		} else {
			boolean insertBankFlag = false;
			for (Banks b : responseBankList) {
				insertBankFlag = true;
				for (SPBank sb : sBankList) {
					if ((b.getCountry().equals(sb.getBankCountry())) && (b.getCode().equals(sb.getBankCode()))) {
						insertBankFlag = false;
						break;
					}
				}
				branchlist = b.getBranches();
				if (insertBankFlag) {
					spBank = new SPBank();
					spBank.setBankCode(b.getCode());
					spBank.setBankCountry(b.getCountry());
					spBank.setBankName(b.getName());

					transactionDao.save(spBank);
					if (branchlist != null) {
						for (Branch b2 : branchlist) {
							spBranch = new SPBranch();
							spBranch.setBranchCode(b2.getCode());
							spBranch.setBranchName(b2.getName());
							spBranch.setSpBank(spBank);

							transactionDao.save(spBranch);
						}
					}
				}
			}
		}
	}

	private void logGetBanks(GetBankResponseDto response, GetBankRequestDto request) throws DaoException {

		List<SPBank> spBankList = (List<SPBank>) transactionDao.getSpBankObject(request);

		List<Banks> responseBankList = response.getBanks();


		SPBranch spBranch = null;
		SPBank spBank = null;

		List<Branch> branchlist = null;

		boolean insertFlag = true;

		if (spBankList.size() == 0 && (responseBankList !=null)) {
			for (Banks b : responseBankList) {
				spBank = new SPBank();
				spBank.setBankCode(b.getCode());
				spBank.setBankCountry(b.getCountry());
				spBank.setBankName(b.getName());
				transactionDao.save(spBank);

				branchlist = b.getBranches();
				if (branchlist != null) {
					for (Branch b2 : branchlist) {
						spBranch = new SPBranch();
						spBranch.setBranchCode(b2.getCode());
						spBranch.setBranchName(b2.getName());
						spBranch.setSpBank(spBank);
						transactionDao.save(spBranch);
					}
				}
			}
		} else {
			for (Banks b : responseBankList) {
				insertFlag = true;
				for (SPBank sb : spBankList) {
					if ((b.getCountry().equals(sb.getBankCountry())) && (b.getCode().equals(sb.getBankCode()))) {
						insertFlag = false;
						
						break;
					}
				}

				if (insertFlag) {
					spBank = new SPBank();
					spBank.setBankCode(b.getCode());
					spBank.setBankCountry(b.getCountry());
					spBank.setBankName(b.getName());

					transactionDao.save(spBank);
					branchlist = b.getBranches();
					if (branchlist != null) {
						for (Branch b2 : branchlist) {
							spBranch = new SPBranch();
							spBranch.setBranchCode(b2.getCode());
							spBranch.setBranchName(b2.getName());
							spBranch.setSpBank(spBank);
							transactionDao.save(spBranch);
						}
					}
				}
			}
		}

	}
}
