package com.mfs.client.simba.service;

import com.mfs.client.simba.dto.GetStatusRequestDto;
import com.mfs.client.simba.dto.GetStatusResponseDto;

public interface TransactionStatusService {

	GetStatusResponseDto getStatus(GetStatusRequestDto request);

}
