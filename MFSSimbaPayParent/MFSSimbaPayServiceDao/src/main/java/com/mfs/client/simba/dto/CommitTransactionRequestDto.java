package com.mfs.client.simba.dto;

public class CommitTransactionRequestDto {

	private String quoteReference;

	public String getQuoteReference() {
		return quoteReference;
	}

	public void setQuoteReference(String quoteReference) {
		this.quoteReference = quoteReference;
	}

	@Override
	public String toString() {
		return "CommitTransactionRequestDto [quoteReference=" + quoteReference + "]";
	}
	
}
