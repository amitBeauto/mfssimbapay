package com.mfs.client.simba.dto;

public class Recipient {
	
	private String firstName;
	private String lastName;
	private String mobile;
	private Address address;
	private String email;
	private Bank bankDetails;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Bank getBankDetails() {
		return bankDetails;
	}
	public void setBankDetails(Bank bankDetails) {
		this.bankDetails = bankDetails;
	}
	@Override
	public String toString() {
		return "Recipient [firstName=" + firstName + ", lastName=" + lastName + ", mobile=" + mobile + ", address="
				+ address + ", email=" + email + ", bankDetails=" + bankDetails + "]";
	}
	
}
