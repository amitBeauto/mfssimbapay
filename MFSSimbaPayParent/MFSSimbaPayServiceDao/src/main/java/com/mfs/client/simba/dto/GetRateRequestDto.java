package com.mfs.client.simba.dto;

public class GetRateRequestDto {

	private String sourceCountry;
	private String destinationCountry;

	public String getSourceCountry() {
		return sourceCountry;
	}

	public void setSourceCountry(String sourceCountry) {
		this.sourceCountry = sourceCountry;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	@Override
	public String toString() {
		return "GetRateRequestDto [sourceCountry=" + sourceCountry + ", destinationCountry=" + destinationCountry + "]";
	}

}
