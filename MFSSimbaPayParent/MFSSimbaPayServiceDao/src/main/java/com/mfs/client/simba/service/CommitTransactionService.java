package com.mfs.client.simba.service;

import com.mfs.client.simba.dto.CommitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;

public interface CommitTransactionService {

	public InitTransactionResponseDto commitTransaction(CommitTransactionRequestDto req, InitTransactionRequestDto reqest);
}
