package com.mfs.client.simba.service.impl;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.simba.service.impl.BaseServiceImpl;
import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.CommitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPGetQuote;
import com.mfs.client.simba.model.SPTransactionError;
import com.mfs.client.simba.model.SPTransactionLog;
import com.mfs.client.simba.service.CommitTransactionService;
import com.mfs.client.simba.service.InitiateTransactionService;
import com.mfs.client.simba.util.CallServices;
import com.mfs.client.simba.util.CommonConstant;
import com.mfs.client.simba.util.CommonValidations;
import com.mfs.client.simba.util.EncryptSHA256Util;
import com.mfs.client.simba.util.MFSSPCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("InitiateTransactionService")
public class InitiateTransactionServiceImpl extends BaseServiceImpl implements InitiateTransactionService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;

	@Autowired
	CommitTransactionService commitTransactionService;

	private static final Logger LOGGER = Logger.getLogger(InitiateTransactionServiceImpl.class);

	public InitTransactionResponseDto initTransaction(InitTransactionRequestDto request) {
		LOGGER.debug("Inside InitiateTransactionServiceImpl --> initTransaction" + request);

		InitTransactionResponseDto response = null;
		String jsonResponse;
		String agentReference;
		Gson gson = new Gson();
		String paymentMethod = "";
		CommonValidations validate = new CommonValidations();
		boolean validateFlag = false;
		Date date = new Date();
		long nonce = date.getTime();
		String signature = " ";
		CommitTransactionRequestDto req = new CommitTransactionRequestDto();

		logTransSession(request.getTransaction().getQuoteReference(), request.getTransaction().getAgentReference(), "",
				request.getTransaction().getCallBackUrl(), request.getTransaction().getSender().getFirstName(),
				request.getTransaction().getSender().getLastName(), request.getTransaction().getSender().getMobile(),
				request.getTransaction().getSender().getAddress().getPostcode(),
				request.getTransaction().getSender().getAddress().getCountry(),
				request.getTransaction().getRecipient().getFirstName(),
				request.getTransaction().getRecipient().getLastName(),
				request.getTransaction().getRecipient().getMobile(),
				request.getTransaction().getRecipient().getAddress().getCountry(),
				request.getTransaction().getRecipient().getBankDetails().getAccountNumber(),
				request.getTransaction().getRecipient().getBankDetails().getBankCode(), "Initiated",
				"Transaction Initiated", new Date(), request.getTransaction().getSender().getId().getType(),
				request.getTransaction().getSender().getId().getNumber(),
				request.getTransaction().getSender().getId().getIssuedBy(),
				request.getTransaction().getSender().getId().getStartDate(),
				request.getTransaction().getSender().getId().getExpiryDate());

		try {
			Map<String, String> systemConfigDetails = systemConfigDetailsDao.getConfigDetailsMap();
			if (systemConfigDetails == null) {
				throw new Exception("system config details not in DB");
			}
			String companyId = systemConfigDetails.get(CommonConstant.COMPANY_ID);
			// check for payment method using getQuote
			SPGetQuote quoteModel = transactionDao
					.getTransationByQuoteReference(request.getTransaction().getQuoteReference());

			paymentMethod = quoteModel.getPaymentMethod();
			if (paymentMethod.equalsIgnoreCase("BANK")) {
				if (validate.validateStringValues(
						request.getTransaction().getRecipient().getBankDetails().getAccountNumber())) {
					response = new InitTransactionResponseDto();
					response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
					response.setMessage(
							MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_ACCOUNT_NO);
				} else if (validate
						.validateStringValues(request.getTransaction().getRecipient().getBankDetails().getBankCode())) {
					response = new InitTransactionResponseDto();
					response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
					response.setMessage(
							MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_BANK_CODE);
				} else if (validate
						.validateStringValues(request.getTransaction().getRecipient().getBankDetails().getCountry())) {
					response = new InitTransactionResponseDto();
					response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
					response.setMessage(
							MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_COUNTRY);
				} else {
					validateFlag = true;
				}
			} else {
				validateFlag = true;
			}

			if (validateFlag) {
				// check wheather mfstrnsid is already present
				agentReference = request.getTransaction().getAgentReference();
				SPTransactionLog obj = transactionDao.getTransationByTransId(agentReference);

				if (obj != null) {
					response = new InitTransactionResponseDto();
					response.setCode(MFSSPCode.SPCODE_ERROR_TNX_DUPLICATE.getCode());
					response.setMessage(MFSSPCode.SPCODE_ERROR_TNX_DUPLICATE.getMessage());
				} else {

					saveInitTransaction(request);

					signature = EncryptSHA256Util.EncryptSHA256Alogo(nonce,
							systemConfigDetails.get(CommonConstant.COMPANY_ID),
							systemConfigDetails.get(CommonConstant.SECRET));
					LOGGER.info("==> InitiateTransactionServiceImpl -->initTransaction -->Signature: " + signature);

					LOGGER.info("==> Initiate Request : " + gson.toJson(request));
					LOGGER.info("==> Sending Request to Url : " + systemConfigDetails.get(CommonConstant.BASE_URL)
							+ systemConfigDetails.get(CommonConstant.INIT_URL));

					jsonResponse = CallServices.getResponseFromService(
							systemConfigDetails.get(CommonConstant.BASE_URL)
									+ systemConfigDetails.get(CommonConstant.INIT_URL),
							gson.toJson(request), signature, nonce, companyId);
					LOGGER.info("In InitServiceImpl Response from HTTP Method : " + jsonResponse);

					response = (InitTransactionResponseDto) JSONToObjectConversion.getObjectFromJson(jsonResponse,
							InitTransactionResponseDto.class);

					LOGGER.info("==>In InitServiceImpl Response from HTTP Method : " + response);
					LOGGER.info("==>In InitServiceImpl String Response from HTTP method :" + response.toString());

					updateInitTransaction(response, request.getTransaction().getAgentReference(), request);

					req.setQuoteReference(response.getQuoteReference());

					// check if response is OK and call commitTransaction
					if (response.getStatus() != null && response.getStatus().equalsIgnoreCase("OK")) {
						response = commitTransactionService.commitTransaction(req, request);
					} else {
						errorInitTransaction(request, MFSSPCode.SPCODE_ERROR_TIMEOUT.getCode(),
								MFSSPCode.SPCODE_ERROR_TIMEOUT.getMessage());
					}

				}

				logTransSession(request.getTransaction().getQuoteReference(),
						request.getTransaction().getAgentReference(), response.getTransactionReference(),
						request.getTransaction().getCallBackUrl(), request.getTransaction().getSender().getFirstName(),
						request.getTransaction().getSender().getLastName(),
						request.getTransaction().getSender().getMobile(),
						request.getTransaction().getSender().getAddress().getPostcode(),
						request.getTransaction().getSender().getAddress().getCountry(),
						request.getTransaction().getRecipient().getFirstName(),
						request.getTransaction().getRecipient().getLastName(),
						request.getTransaction().getRecipient().getMobile(),
						request.getTransaction().getRecipient().getAddress().getCountry(),
						request.getTransaction().getRecipient().getBankDetails().getAccountNumber(),
						request.getTransaction().getRecipient().getBankDetails().getBankCode(), response.getStatus(),
						response.getMessage(), new Date(), request.getTransaction().getSender().getId().getType(),
						request.getTransaction().getSender().getId().getNumber(), request.getTransaction().getSender().getId().getIssuedBy(),
						request.getTransaction().getSender().getId().getStartDate(),
						request.getTransaction().getSender().getId().getExpiryDate());

			} // flag if ends
		} catch (DaoException de) {
			LOGGER.error("exception Occur in  --> initTransaction" + de);
			response = new InitTransactionResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("exception Occur in  --> initTransaction" + e);

			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.SPCODE_ERROR_TIMEOUT.getCode());
			response.setMessage(MFSSPCode.SPCODE_ERROR_TIMEOUT.getMessage());
		}

		return response;
	}

	public void saveInitTransaction(InitTransactionRequestDto request) throws DaoException {
		SPTransactionLog logModel = new SPTransactionLog();

		logModel.setSenderFirstName(request.getTransaction().getSender().getFirstName());
		logModel.setSenderLastName(request.getTransaction().getSender().getLastName());
		logModel.setSenderMobile(request.getTransaction().getSender().getMobile());
		logModel.setSenderAddressLine1(request.getTransaction().getSender().getAddress().getAddressLine1());
		logModel.setSenderAddressLine2(request.getTransaction().getSender().getAddress().getAddressLine2());
		logModel.setSenderCity(request.getTransaction().getSender().getAddress().getCity());
		logModel.setSendertState(request.getTransaction().getSender().getAddress().getState());
		logModel.setSenderCountry(request.getTransaction().getSender().getAddress().getCountry());
		logModel.setSenderPostcode(request.getTransaction().getSender().getAddress().getPostcode());
		logModel.setSenderDateOfBirth(request.getTransaction().getSender().getDateOfBirth());
		logModel.setSenderEmail(request.getTransaction().getSender().getEmail());
		logModel.setSenderNationality(request.getTransaction().getSender().getNationality());

		logModel.setRecipientFirstName(request.getTransaction().getRecipient().getFirstName());
		logModel.setRecipientLastName(request.getTransaction().getRecipient().getLastName());
		logModel.setRecipientMobile(request.getTransaction().getRecipient().getMobile());
		logModel.setRecipientAddressLine1(request.getTransaction().getRecipient().getAddress().getAddressLine1());
		logModel.setRecipientAddressLine2(request.getTransaction().getRecipient().getAddress().getAddressLine2());
		logModel.setRecipientCity(request.getTransaction().getRecipient().getAddress().getCity());
		logModel.setRecipientState(request.getTransaction().getRecipient().getAddress().getState());
		logModel.setRecipientPostcode(request.getTransaction().getRecipient().getAddress().getPostcode());
		logModel.setRecipientCountry(request.getTransaction().getRecipient().getAddress().getCountry());
		logModel.setRecipientEmail(request.getTransaction().getRecipient().getEmail());

		logModel.setBankCode(request.getTransaction().getRecipient().getBankDetails().getBankCode());
		logModel.setBranchCode(request.getTransaction().getRecipient().getBankDetails().getBranchCode());
		logModel.setAccountNumber(request.getTransaction().getRecipient().getBankDetails().getAccountNumber());

		logModel.setType(request.getTransaction().getSender().getId().getType());
		logModel.setNumber(request.getTransaction().getSender().getId().getNumber());
		logModel.setIssuedBy(request.getTransaction().getSender().getId().getIssuedBy());
		logModel.setStartDate(request.getTransaction().getSender().getId().getStartDate());
		logModel.setExpiryDate(request.getTransaction().getSender().getId().getExpiryDate());

		logModel.setQuoteReference(request.getTransaction().getQuoteReference());
		logModel.setMfsTransId(request.getTransaction().getAgentReference());
		logModel.setCallBackUrl(request.getTransaction().getCallBackUrl());

		logModel.setStatus(CommonConstant.SAVE_STATUS);
		logModel.setMessage(CommonConstant.SAVE_STATUS_MSG);

		transactionDao.save(logModel);
	}

	public void updateInitTransaction(InitTransactionResponseDto response, String mfsTransId,
			InitTransactionRequestDto request) throws DaoException {
		SPTransactionLog spLog = transactionDao.getTransationByTransId(mfsTransId);

		if (spLog != null) {
			if (response == null) {
				spLog.setStatus(CommonConstant.ERROR_STATUS);
				spLog.setMessage(MFSSPCode.ER206.getCode() + " " + MFSSPCode.ER206.getMessage());
				transactionDao.update(spLog);

				errorInitTransaction(request, MFSSPCode.ER206.getCode(), MFSSPCode.ER206.getMessage());
			}
			spLog.setQuoteReference(response.getQuoteReference());
			spLog.setStatus(response.getStatus());
			spLog.setMessage(response.getMessage());
			transactionDao.update(spLog);
		}
	}

	public void errorInitTransaction(InitTransactionRequestDto request, String code, String msg) throws DaoException {
		SPTransactionError errModel = new SPTransactionError();

		errModel.setQuoteReference(request.getTransaction().getQuoteReference());
		errModel.setMfsTransId(request.getTransaction().getAgentReference());
		errModel.setCallBackUrl(request.getTransaction().getCallBackUrl());

		errModel.setSenderFirstName(request.getTransaction().getSender().getFirstName());
		errModel.setSenderLastName(request.getTransaction().getSender().getLastName());
		errModel.setSenderMobile(request.getTransaction().getSender().getMobile());
		errModel.setSenderCountry(request.getTransaction().getSender().getAddress().getCountry());

		errModel.setRecipientFirstName(request.getTransaction().getRecipient().getFirstName());
		errModel.setRecipientLastName(request.getTransaction().getRecipient().getLastName());
		errModel.setRecipientMobile(request.getTransaction().getRecipient().getMobile());
		errModel.setRecipientCountry(request.getTransaction().getRecipient().getAddress().getCountry());

		errModel.setAccountNumber(request.getTransaction().getRecipient().getBankDetails().getAccountNumber());
		errModel.setBankCode(request.getTransaction().getRecipient().getBankDetails().getBankCode());

		errModel.setType(request.getTransaction().getSender().getId().getType());
		errModel.setNumber(request.getTransaction().getSender().getId().getNumber());
		errModel.setIssuedBy(request.getTransaction().getSender().getId().getIssuedBy());
		errModel.setStartDate(request.getTransaction().getSender().getId().getStartDate());
		errModel.setExpiryDate(request.getTransaction().getSender().getId().getExpiryDate());

		errModel.setStatus(code);
		errModel.setMessage(msg);

		transactionDao.save(errModel);

	}
}
