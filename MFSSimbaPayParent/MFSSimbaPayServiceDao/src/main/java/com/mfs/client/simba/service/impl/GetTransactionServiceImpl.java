package com.mfs.client.simba.service.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.GetTransactionRequestDto;
import com.mfs.client.simba.dto.GetTransactionResponseDto;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPTransactionLog;
import com.mfs.client.simba.service.GetTransactionService;
import com.mfs.client.simba.util.CallServices;
import com.mfs.client.simba.util.CommonConstant;
import com.mfs.client.simba.util.EncryptSHA256Util;
import com.mfs.client.simba.util.MFSSPCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("GetTransactionService")
public class GetTransactionServiceImpl implements GetTransactionService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;

	private static final Logger LOGGER = Logger.getLogger(GetTransactionServiceImpl.class);

	public GetTransactionResponseDto getTransaction(GetTransactionRequestDto request)  {
		LOGGER.debug("Inside GetTransactionServiceImpl --> getTransactionServiceImpl" + request);

		GetTransactionResponseDto response = null;
		String jsonResponse;
		long nounce = 1555908857133L;
		String signature = null;
		Gson gson = new Gson();

		
		try {
			Map<String, String> systemConfigDetails = systemConfigDetailsDao.getConfigDetailsMap();
			
			if(systemConfigDetails==null)
			{
				throw new Exception("No config Details In DB");
			}
			String companyId = systemConfigDetails.get(CommonConstant.COMPANY_ID);
			signature = EncryptSHA256Util.EncryptSHA256Alogo(nounce, systemConfigDetails.get(CommonConstant.COMPANY_ID),
					systemConfigDetails.get(CommonConstant.SECRET));
			LOGGER.debug("Inside CommitTransactionServiceImpl --> commitTransaction  -->Signature :" + signature);

			jsonResponse = CallServices.getResponseFromService(systemConfigDetails.get(CommonConstant.BASE_URL)
					+ systemConfigDetails.get(CommonConstant.GET_TRANS_URL), gson.toJson(request),signature,nounce, companyId);
			LOGGER.info("In  GetTransactionServiceImpl Response from HTTP Method : " + jsonResponse);

			response = (GetTransactionResponseDto) JSONToObjectConversion.getObjectFromJson(jsonResponse,
					GetTransactionResponseDto.class);

			LOGGER.info("==>In GetTransactionServiceImpl Response from HTTP Method : " + response);
			LOGGER.info("==>In GetTransactionServiceImpl String Response from HTTP method :" + response.toString());

			updateGetTransaction(response);

		} catch (DaoException de) {

			LOGGER.error("exception Occur in  --> getTransaction" + de);

			response = new GetTransactionResponseDto();
			response.setStatus(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());

		} catch (Exception e) {

			LOGGER.error("exception Occur in  --> getTransaction" + e);
			response = new GetTransactionResponseDto();
			response.setStatus(MFSSPCode.SPCODE_ERROR_TIMEOUT.getCode());
			response.setMessage(MFSSPCode.SPCODE_ERROR_TIMEOUT.getMessage());

		}

		return response;
	}

	private void updateGetTransaction(GetTransactionResponseDto response) throws DaoException {
		GetTransactionResponseDto getTransactionResponseDto = null;
		if (response != null) {

			String id = response.getTransactionReference();
			

			SPTransactionLog spTransLogModel = (SPTransactionLog) transactionDao.getTransationByTransRef(id);

			if (spTransLogModel != null) {

				spTransLogModel.setTransactionReference(response.getTransactionReference());
				spTransLogModel.setStatus(response.getStatus());
				spTransLogModel.setMessage(response.getMessage());

				try {
					transactionDao.update(spTransLogModel);
				} catch (DaoException e) {
					e.printStackTrace();
				}
			}

		} else {
			getTransactionResponseDto=new GetTransactionResponseDto();
			getTransactionResponseDto.setStatus(MFSSPCode.ER203.getCode());
			getTransactionResponseDto.setMessage(MFSSPCode.ER203.getMessage());

		}

	}

	

}
