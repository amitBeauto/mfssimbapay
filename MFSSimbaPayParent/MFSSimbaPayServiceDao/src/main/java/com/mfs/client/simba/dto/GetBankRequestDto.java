package com.mfs.client.simba.dto;

public class GetBankRequestDto {

	private String country;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "GetBankRequestDto [country=" + country + "]";
	}

}
