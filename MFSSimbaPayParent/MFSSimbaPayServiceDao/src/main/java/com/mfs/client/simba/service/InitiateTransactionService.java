package com.mfs.client.simba.service;

import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;

public interface InitiateTransactionService {

	InitTransactionResponseDto initTransaction(InitTransactionRequestDto request);

}
