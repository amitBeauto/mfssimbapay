package com.mfs.client.simba.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sp_get_rates")
public class SPGetRates {

	@Id
	@GeneratedValue
	@Column(name = "trans_rates_id")
	private long transRatesId;

	@Column(name = "source_country")
	private String sourceCountry;

	@Column(name = "source_currency")
	private String sourceCurrency;

	@Column(name = "destination_country")
	private String destinationCountry;

	@Column(name = "destination_currency")
	private String destinationCurrency;

	@OneToMany( mappedBy = "spGetRates", cascade =CascadeType.ALL)
	private List<SPDelivery> deliveries;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "spGetRates")
	private List<SPPayment> payments;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "spGetRates")
	private List<SPTime> times;

	public long getTransRatesId() {
		return transRatesId;
	}

	public void setTransRatesId(long transRatesId) {
		this.transRatesId = transRatesId;
	}

	public String getSourceCountry() {
		return sourceCountry;
	}

	public void setSourceCountry(String sourceCountry) {
		this.sourceCountry = sourceCountry;
	}

	public String getSourceCurrency() {
		return sourceCurrency;
	}

	public void setSourceCurrency(String sourceCurrency) {
		this.sourceCurrency = sourceCurrency;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getDestinationCurrency() {
		return destinationCurrency;
	}

	public void setDestinationCurrency(String destinationCurrency) {
		this.destinationCurrency = destinationCurrency;
	}

	public List<SPDelivery> getDeliveries() {
		return deliveries;
	}

	public void setDeliveries(List<SPDelivery> deliveries) {
		this.deliveries = deliveries;
	}

	public List<SPPayment> getPayments() {
		return payments;
	}

	public void setPayments(List<SPPayment> payments) {
		this.payments = payments;
	}

	public List<SPTime> getTimes() {
		return times;
	}

	public void setTimes(List<SPTime> times) {
		this.times = times;
	}

	@Override
	public String toString() {
		return "SPGetRates [transRatesId=" + transRatesId + ", sourceCountry=" + sourceCountry + ", sourceCurrency="
				+ sourceCurrency + ", destinationCountry=" + destinationCountry + ", destinationCurrency="
				+ destinationCurrency + ", deliveries=" + deliveries + ", payments=" + payments + ", times=" + times
				+ "]";
	}

}
