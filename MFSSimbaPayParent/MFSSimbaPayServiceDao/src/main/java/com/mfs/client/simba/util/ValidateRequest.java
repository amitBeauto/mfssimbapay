package com.mfs.client.simba.util;

import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;

public class ValidateRequest {

	public InitTransactionResponseDto validateInitRequest(InitTransactionRequestDto request) {
		CommonValidations validate = new CommonValidations();
		InitTransactionResponseDto response = null;

		if (validate.validateStringValues(request.getTransaction().getQuoteReference())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_QUOTE_REFERENCE);
		} else if (validate.validateStringValues(request.getTransaction().getAgentReference())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_AGENT_REFERENCE);
		} else if (validate.validateStringValues(request.getTransaction().getCallBackUrl())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_CALLBACK_URL);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getFirstName())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(
					MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_FIRST_NAME);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getLastName())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(
					MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_LAST_NAME);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getMobile())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_MOBILE);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getDateOfBirth())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_DOB);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getAddress().getAddressLine1())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_ADDRESS);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getAddress().getCity())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_CITY);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getAddress().getPostcode())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_POSTCODE);
		} else if (validate.validateStringValues(request.getTransaction().getSender().getAddress().getCountry())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_COUNTRY);
		} else if (validate.validateStringValues(request.getTransaction().getRecipient().getFirstName())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(
					MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_FIRST_NAME);
		} else if (validate.validateStringValues(request.getTransaction().getRecipient().getLastName())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(
					MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_LAST_NAME);
		} else if (validate.validateStringValues(request.getTransaction().getRecipient().getMobile())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_MOBILE);
		}else if (validate.validateStringValues(request.getTransaction().getSender().getId().getType())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ID_TYPE);
		}else if (validate.validateStringValues(request.getTransaction().getSender().getId().getNumber())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ID_NUMBER);
		}else if (validate.validateStringValues(request.getTransaction().getSender().getId().getIssuedBy())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ISSUED_BY);
		}else if (validate.validateStringValues(request.getTransaction().getSender().getId().getStartDate())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_START_DATE);
		}else if (validate.validateStringValues(request.getTransaction().getSender().getId().getExpiryDate())) {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_EXPIRY_DATE);
		}

		return response;

	}

}
