package com.mfs.client.simba.service;

import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetQuoteResponseDto;
import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetRateResponseDto;

public interface GetRatesService {

	public GetRateResponseDto getRates(GetRateRequestDto getRateRequestDto);

}
