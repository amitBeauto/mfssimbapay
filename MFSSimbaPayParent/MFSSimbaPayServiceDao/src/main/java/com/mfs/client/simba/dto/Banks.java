package com.mfs.client.simba.dto;

import java.util.List;

public class Banks {

	private String code;
	private String name;
	private String country;
	private List<Branch> branches;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<Branch> getBranches() {
		return branches;
	}

	public void setBranches(List<Branch> branches) {
		this.branches = branches;
	}

	@Override
	public String toString() {
		return "Bank [code=" + code + ", name=" + name + ", country=" + country + ", branches=" + branches + "]";
	}

}
