package com.mfs.client.simba.util;

public enum MFSSPCode {

	VALIDATION_ERROR("ER201","Validation Error"),
	SPCODE_ERROR_TIMEOUT("ER202","Connection Timeout"),
	ER203("ER203","Exception while processing your request, Please try again later."),
	ER204("ER204","Exception while save data"),
	ER205("ER205","Exception while update"),
	ER206("ER206","Null Response"),
	ER207("ER207","Exception while getting config details"),
	ER208("ER208","Exception while getting account details"),
	ER209("ER209","Exception while updating account details "),
	ER210("ER210","Exception while getting transaction by mfsTransId "),
	ER211("ER211","Exception while getting transaction by thirdPartyTransId "),
	SPCODE_ERROR_TNX_DUPLICATE("ER212","Duplicate Transaction."),
	ER213("ER213","Exception while getting request"),
	ER214("ER214","Exception while getting transaction by quoteReference"),
	ER215("ER215","Exception while getting transaction by transactionReference "),
	ER216("ER216","Exception while getting quote by quoteReference"),
	ER217("ER217","Exception while getting bank by country"),
	ER218("ER218","Exception while getting quote by country"),
	ER219("ER219","Exception while getting rates by country"),
	ER220("ER220","Null Request");
	
	
	
    private String code;
	private String message;

	private MFSSPCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}


}
