package com.mfs.client.simba.dto;

public class GetTransactionRequestDto {

	private String transactionReference;
	private String agentReference;
	
	public String getTransactionReference() {
		return transactionReference;
	}
	public void setTransactionReference(String transactionReference) {
		this.transactionReference = transactionReference;
	}
	public String getAgentReference() {
		return agentReference;
	}
	public void setAgentReference(String agentReference) {
		this.agentReference = agentReference;
	}
	@Override
	public String toString() {
		return "GetTransactionRequestDto [transactionReference=" + transactionReference + ", agentReference="
				+ agentReference + "]";
	}
	
}
