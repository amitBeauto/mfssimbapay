package com.mfs.client.simba.dao.impl;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetStatusRequestDto;
import com.mfs.client.simba.dto.ResponseStatus;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPBank;
import com.mfs.client.simba.model.SPGetQuote;
import com.mfs.client.simba.model.SPGetRates;
import com.mfs.client.simba.model.SPTransactionLog;
import com.mfs.client.simba.util.MFSSPCode;

@Repository("TransactionDao")
@EnableTransactionManagement
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(SystemConfigDetailsDaoImpl.class);
	
	@Transactional
	public SPGetQuote getQuoteObject(GetQuoteRequestDto request) throws DaoException  {
		LOGGER.debug("Inside getQuoteObject of TransactionDaoImpl request: " + request);
		SPGetQuote spQuote = null;
		try {
			Session session = sessionFactory.openSession();
			spQuote = (SPGetQuote) session.createQuery(
				"From SPGetQuote s where s.sourceCountry =:sourceCountry and s.destinationCountry =:destinationCountry")
				.setParameter("sourceCountry", request.getSourceCountry())
				.setParameter("destinationCountry", request.getDestinationCountry()).uniqueResult();
		}
		catch (Exception e) {
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER218.getCode());
			status.setStatusMessage(MFSSPCode.ER218.getMessage());
			
			throw new DaoException(status);
		}
		return spQuote;
	}

	/*public SPTransactionLog findTransation(String agentReference) {
		Session session = sessionFactory.openSession();
		SPTransactionLog obj = (SPTransactionLog) session
				.createQuery("from SPTransactionLog tr where tr.mfsTransId=:mfsTransId")
				.setParameter("mfsTransId", agentReference);
		return obj;
	}*/

	@Transactional
	public SPGetRates getSpRatesObject(GetRateRequestDto request) throws DaoException{
		LOGGER.debug("Inside getSpBankObject1 of TransactionDaoImpl request: " + request);
		SPGetRates spGetRates = null;
		try {
		Session session = sessionFactory.openSession();
		
		spGetRates = (SPGetRates) session.createQuery("from SPGetRates r where r.sourceCountry =:sourceCountry and r.destinationCountry =:destinationCountry")
						.setParameter("sourceCountry", request.getSourceCountry())
						.setParameter("destinationCountry", request.getDestinationCountry()).uniqueResult();
		}
		catch (Exception e) {
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER219.getCode());
			status.setStatusMessage(MFSSPCode.ER219.getMessage());
			
			throw new DaoException(status);
		}
		return spGetRates;
	}
	
	public List<SPGetRates> getALLSpRatesObject() throws DaoException {
		LOGGER.debug("Inside getALLSpRatesObject of TransactionDaoImpl ");
		List<SPGetRates> spGetRates = null;
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from SPGetRates");
		spGetRates = query.list();
		
		return spGetRates;
	}
	
	
		@Transactional()
		public List<SPBank> getSpBankObject1(GetBankRequestDto request)throws DaoException {

		LOGGER.debug("Inside getSpBankObject1 of TransactionDaoImpl request: " + request);
		List<SPBank> spBanks = null;
		try {
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("from SPBank");
			spBanks = query.list();
		}
		catch (Exception e) {
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER217.getCode());
			status.setStatusMessage(MFSSPCode.ER217.getMessage());
			
			throw new DaoException(status);
		}
		
		return spBanks;
		}
		
		@Transactional
		public List<SPBank> getSpBankObject(GetBankRequestDto request)throws DaoException {

		LOGGER.debug("Inside getSpBankObject1 of TransactionDaoImpl request: " + request);
		List<SPBank> spBanks = null;
		try {
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("from SPBank where bankCountry =:country ").setParameter("country", request.getCountry());
			spBanks = query.list();
		}
		catch (Exception e) {
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER217.getCode());
			status.setStatusMessage(MFSSPCode.ER217.getMessage());
			
			throw new DaoException(status);
		}
		
		return spBanks;
		}
	
	@Transactional
	public SPTransactionLog getTransationByTransId(String mfsTransId) throws DaoException {
		LOGGER.debug("Inside getTransactionByTransId of TransactionDaoImpl request: " + mfsTransId);
		SPTransactionLog spTransactionLog = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			String hql = "From SPTransactionLog where mfsTransId = :mfsTransId";
			Query query = session.createQuery(hql).setParameter("mfsTransId", mfsTransId);

			SPTransactionLog spTransactionLogList = (SPTransactionLog) query.uniqueResult();
		
		
			if(spTransactionLogList != null) {
				spTransactionLog = spTransactionLogList;
			}
		}
		catch(Exception e)
		{
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER210.getCode());
			status.setStatusMessage(MFSSPCode.ER210.getMessage());
			
			throw new DaoException(status);
		}

		LOGGER.debug("Inside getTransactionByTransId of TransactionDaoImpl db response: " + spTransactionLog);
		return spTransactionLog;

	}

	@Transactional
	public SPGetQuote getTransationByQuoteReference(String quoteReference) throws DaoException {
		LOGGER.debug("Inside TransactionDaoImpl --> getTransationByQuoteReference --> request: " + quoteReference);
		SPGetQuote spGetQuote = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			String hql = " From SPGetQuote where quoteReference = :quoteReference";
			Query query = session.createQuery(hql).setParameter("quoteReference", quoteReference);

			List<SPGetQuote> spGetQuoteList = (List<SPGetQuote>) query.list();
		
		
			if(spGetQuoteList != null && !spGetQuoteList.isEmpty()) {
				spGetQuote = spGetQuoteList.get(0);
			}
		}
		catch(Exception e)
		{
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER216.getCode());
			status.setStatusMessage(MFSSPCode.ER216.getMessage());
			
			throw new DaoException(status);
		}

		LOGGER.debug("Inside TransactionDaoImpl -->getTransationByQuoteReference -->db response: " + spGetQuote);
		return spGetQuote;
	}


	@Transactional
	public SPTransactionLog getTransationLogByQuoteReference(String quoteReference) throws DaoException {
		LOGGER.debug("Inside TransactionDaoImpl --> getTransationLogByQuoteReference --> request: " + quoteReference);
		SPTransactionLog spLog = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			String hql = " From SPTransactionLog where quoteReference = :quoteReference";
			Query query = session.createQuery(hql).setParameter("quoteReference", quoteReference);

			 spLog = (SPTransactionLog) query.uniqueResult();
		
		}
		catch(Exception e)
		{
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER214.getCode());
			status.setStatusMessage(MFSSPCode.ER214.getMessage());
			
			throw new DaoException(status);
		}

		LOGGER.debug("Inside TransactionDaoImpl -->getTransationByQuoteReference -->db response: " + spLog);
		return spLog;
	}

	public SPTransactionLog getTransactionByTransReference(GetStatusRequestDto request) throws DaoException{

		LOGGER.debug("Inside getTransactionByTransReference of TransactionDaoImpl request: " + request);
		SPTransactionLog spTransactionLog = null;
		String transactionReference = request.getTransactionReference();
		try
		{
			Session session = sessionFactory.openSession();
			String hql = "From SPTransactionLog where transactionReference = :transactionReference";
			Query query = session.createQuery(hql).setParameter("transactionReference", transactionReference);

			spTransactionLog = (SPTransactionLog) query.uniqueResult();	

		}
		catch(Exception e)
		{
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER215.getCode());
			status.setStatusMessage(MFSSPCode.ER215.getMessage());

			throw new DaoException(status);
		}

		LOGGER.debug("Inside getTransactionByTransReference of TransactionDaoImpl db response: " + spTransactionLog);
		return spTransactionLog;
		
	}
	@Transactional
	public SPTransactionLog getTransationByTransRef(String transactionReference) throws DaoException {
		LOGGER.debug("Inside getTransactionByTransId of TransactionDaoImpl request: " +transactionReference);
		SPTransactionLog spTransactionLog = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			String hql = "From SPTransactionLog where transactionReference = :transactionReference";
			Query query = session.createQuery(hql).setParameter("transactionReference", transactionReference);

			SPTransactionLog spTransactionLogList = (SPTransactionLog) query.uniqueResult();
		
		
			if(spTransactionLogList != null) {
				spTransactionLog = spTransactionLogList;
			}
		}
		catch(Exception e)
		{
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER210.getCode());
			status.setStatusMessage(MFSSPCode.ER210.getMessage());
			
			throw new DaoException(status);
		}

		LOGGER.debug("Inside getTransactionByTransId of TransactionDaoImpl db response: " + spTransactionLog);
		return spTransactionLog;

	}
	


}
