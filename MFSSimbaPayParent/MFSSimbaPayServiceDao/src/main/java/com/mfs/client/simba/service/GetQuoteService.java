package com.mfs.client.simba.service;

import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetQuoteResponseDto;

public interface GetQuoteService {

	GetQuoteResponseDto getQuote(GetQuoteRequestDto request);

}
