package com.mfs.client.simba.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sp_bank")
public class SPBank {

	@Id
	@GeneratedValue
	@Column(name = "trans_bank_id")
	private long transBankId;

	@Column(name = "bank_code")
	private String bankCode;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "bank_country")
	private String bankCountry;

	@OneToMany(cascade = CascadeType.ALL, mappedBy ="spBank")
	private List<SPBranch> branches;

	public long getTransBankId() {
		return transBankId;
	}

	public void setTransBankId(long transBankId) {
		this.transBankId = transBankId;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankCountry() {
		return bankCountry;
	}

	public void setBankCountry(String bankCountry) {
		this.bankCountry = bankCountry;
	}

	
	public List<SPBranch> getBranches() {
		return branches;
	}

	public void setBranches(List<SPBranch> branches) {
		this.branches = branches;
	}

	@Override
	public String toString() {
		return "SPBank [transBankId=" + transBankId + ", bankCode=" + bankCode + ", bankName=" + bankName
				+ ", bankCountry=" + bankCountry + ", branches=" + branches + "]";
	}

}
