package com.mfs.client.simba.service.impl;

import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.CommitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPTransactionError;
import com.mfs.client.simba.model.SPTransactionLog;
import com.mfs.client.simba.service.CommitTransactionService;
import com.mfs.client.simba.util.CallServices;
import com.mfs.client.simba.util.CommonConstant;
import com.mfs.client.simba.util.EncryptSHA256Util;
import com.mfs.client.simba.util.MFSSPCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("CommitTransactionService")
public class CommitTransactionServiceImpl implements CommitTransactionService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;

	private static final Logger LOGGER = Logger.getLogger(InitiateTransactionServiceImpl.class);

	public InitTransactionResponseDto commitTransaction(CommitTransactionRequestDto req, InitTransactionRequestDto request) {
		LOGGER.debug("Inside CommitTransactionServiceImpl --> commitTransaction" + req);

		String jsonResponse;
		InitTransactionResponseDto response = null;
		Date date = new Date();
		long nonce = date.getTime();
		String signature = " ";
		Gson gson = new Gson();

		try {
			Map<String, String> systemConfigDetails = systemConfigDetailsDao.getConfigDetailsMap();
			if(systemConfigDetails==null)
			{
				throw new Exception("system config details not in DB");
			}
			String companyId = systemConfigDetails.get(CommonConstant.COMPANY_ID);
			signature = EncryptSHA256Util.EncryptSHA256Alogo(nonce, 
					systemConfigDetails.get(CommonConstant.COMPANY_ID), systemConfigDetails.get(CommonConstant.SECRET));
			LOGGER.debug("Inside CommitTransactionServiceImpl --> commitTransaction  -->Signature :" + signature);
			
			SPTransactionLog spLog = transactionDao.getTransationLogByQuoteReference(req.getQuoteReference());

			if (spLog != null) {
				
				LOGGER.info("==> Commit Request : " + req.getQuoteReference());
				LOGGER.info("==> Sending Request to Url : " + systemConfigDetails.get(CommonConstant.BASE_URL)
					+ systemConfigDetails.get(CommonConstant.COMMIT_URL));
				
				jsonResponse = CallServices.getResponseFromService(
						systemConfigDetails.get(CommonConstant.BASE_URL) + systemConfigDetails.get(CommonConstant.COMMIT_URL), gson.toJson(req), signature,
						nonce,companyId);
				LOGGER.info("In CommitServiceImpl Response from HTTP Method : "+ jsonResponse);
				
				response = (InitTransactionResponseDto) JSONToObjectConversion.getObjectFromJson(jsonResponse,
						InitTransactionResponseDto.class);
				
				LOGGER.info("==>In CommitServiceImpl Response from HTTP Method : "+ response);
				LOGGER.info("==>In CommitServiceImpl String Response from HTTP method :"+ response.toString());

				response = updateCommitTransaction(response, req.getQuoteReference(), request);
			}
		} catch (DaoException de) {
			
			LOGGER.error("exception Occur in  --> commitTransaction" + de);
			response = new InitTransactionResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
			
		} catch (Exception e) {
			
			LOGGER.error("exception Occur in  --> commitTransaction" + e);
			response = new InitTransactionResponseDto();
			response.setMessage(MFSSPCode.SPCODE_ERROR_TIMEOUT.getMessage());
			response.setCode(MFSSPCode.SPCODE_ERROR_TIMEOUT.getCode());
		}

		return response;
	}

	public InitTransactionResponseDto updateCommitTransaction(InitTransactionResponseDto response, String quoteRef, InitTransactionRequestDto request)
			throws DaoException {
		SPTransactionLog spLog = transactionDao.getTransationLogByQuoteReference(quoteRef);

		if (spLog != null) {
			if (response == null) {
				spLog.setStatus(CommonConstant.ERROR_STATUS);
				spLog.setMessage(MFSSPCode.ER206.getCode() + " " + MFSSPCode.ER206.getMessage());
				transactionDao.update(spLog);

				errorCommitTransaction(request, MFSSPCode.ER206.getCode(), MFSSPCode.ER206.getMessage(), quoteRef);
			}
			spLog.setTransactionReference(response.getTransactionReference());
			spLog.setStatus(response.getStatus());
			spLog.setMessage(response.getMessage());
			transactionDao.update(spLog);
		}
		return response;
	}

	public void errorCommitTransaction(InitTransactionRequestDto request, String code, String msg , String quoteRef) throws DaoException
	{
		SPTransactionError errModel = new SPTransactionError();
		//SPTransactionLog spLog = transactionDao.getTransationLogByQuoteReference(quoteRef);
		
		errModel.setQuoteReference(request.getTransaction().getQuoteReference());
		errModel.setMfsTransId(request.getTransaction().getAgentReference());
		errModel.setCallBackUrl(request.getTransaction().getCallBackUrl());
		
		errModel.setSenderFirstName(request.getTransaction().getSender().getFirstName());
		errModel.setSenderLastName(request.getTransaction().getSender().getLastName());
		errModel.setSenderMobile(request.getTransaction().getSender().getMobile());
		errModel.setSenderCountry(request.getTransaction().getSender().getAddress().getCountry());
		
		errModel.setRecipientFirstName(request.getTransaction().getRecipient().getFirstName());
		errModel.setRecipientLastName(request.getTransaction().getRecipient().getLastName());
		errModel.setRecipientMobile(request.getTransaction().getRecipient().getMobile());
		errModel.setRecipientCountry(request.getTransaction().getRecipient().getAddress().getCountry());
		
		errModel.setAccountNumber(request.getTransaction().getRecipient().getBankDetails().getAccountNumber());
		errModel.setBankCode(request.getTransaction().getRecipient().getBankDetails().getBankCode());
		
		errModel.setType(request.getTransaction().getSender().getId().getType());
		errModel.setNumber(request.getTransaction().getSender().getId().getNumber());
		errModel.setIssuedBy(request.getTransaction().getSender().getId().getIssuedBy());
		errModel.setStartDate(request.getTransaction().getSender().getId().getStartDate());
		errModel.setExpiryDate(request.getTransaction().getSender().getId().getExpiryDate());
		
		errModel.setStatus(code);
		errModel.setMessage(msg);
		
		transactionDao.save(errModel);
		
	}
}
