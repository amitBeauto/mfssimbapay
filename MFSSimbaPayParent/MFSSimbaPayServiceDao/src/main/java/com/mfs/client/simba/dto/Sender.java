package com.mfs.client.simba.dto;

public class Sender {

	private String firstName;
	private String lastName;
	private String mobile;
	private Id id;
	private Address address;
	private String dateOfBirth;
	private String email;
	private String nationality;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Id getId() {
		return id;
	}

	public void setId(Id id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Sender [firstName=" + firstName + ", lastName=" + lastName + ", mobile=" + mobile + ", id=" + id
				+ ", address=" + address + ", dateOfBirth=" + dateOfBirth + ", email=" + email + ", nationality="
				+ nationality + "]";
	}

}
