package com.mfs.client.simba.service;

import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetBankResponseDto;

public interface GetBanksService {

	public GetBankResponseDto getBankDetails(GetBankRequestDto request);

}
