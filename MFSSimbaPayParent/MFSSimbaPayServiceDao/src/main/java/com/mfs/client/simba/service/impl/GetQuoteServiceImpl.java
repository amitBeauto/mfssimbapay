package com.mfs.client.simba.service.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetQuoteResponseDto;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPGetQuote;
import com.mfs.client.simba.service.GetQuoteService;
import com.mfs.client.simba.util.CallServices;
import com.mfs.client.simba.util.CommonConstant;
import com.mfs.client.simba.util.EncryptSHA256Util;
import com.mfs.client.simba.util.MFSSPCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("GetQuoteService")
public class GetQuoteServiceImpl implements GetQuoteService {
	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;

	private static final Logger LOGGER = Logger.getLogger(GetQuoteServiceImpl.class);

	public GetQuoteResponseDto getQuote(GetQuoteRequestDto request) {
		LOGGER.info("==>In GetQuoteServiceImpl in getQuote function  =" + request);

		String jsonResponse;
		GetQuoteResponseDto quoteResponse = null;
		long nounce = 1555908857133L;
		String signature = null;
		Gson gson = new Gson();

		try {

			logQuote(request);

			Map<String, String> systemConfig = null;
			systemConfig = systemConfigDetailsDao.getConfigDetailsMap();
			if (systemConfig == null) {
				throw new Exception("No Config Details in DB");
			}
			String companyId = systemConfig.get(CommonConstant.COMPANY_ID);
			signature = EncryptSHA256Util.EncryptSHA256Alogo(nounce, systemConfig.get(CommonConstant.COMPANY_ID),
					systemConfig.get(CommonConstant.SECRET));

			LOGGER.info("==> GetQuoteServiceImpl -->getQuote -->Signature: " + signature);

			LOGGER.info("==> function Request : " + gson.toJson(request));
			LOGGER.info("==> Sending Request to Url : " + systemConfig.get(CommonConstant.BASE_URL));

			jsonResponse = CallServices.getResponseFromService(
					systemConfig.get(CommonConstant.BASE_URL) + systemConfig.get(CommonConstant.QUOTE_URL),
					gson.toJson(request), signature, nounce, companyId);

			quoteResponse = (GetQuoteResponseDto) JSONToObjectConversion.getObjectFromJson(jsonResponse,
					GetQuoteResponseDto.class);

			LOGGER.info("==> function response : " + jsonResponse);
			if (quoteResponse != null) {
				updateGetQuote(request, quoteResponse);

			} else {
				quoteResponse = new GetQuoteResponseDto();
				quoteResponse.setCode(MFSSPCode.ER203.getCode());
				quoteResponse.setMessage(MFSSPCode.ER203.getMessage());
			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetQuoteServiceImpl" + de);
			quoteResponse = new GetQuoteResponseDto();
			quoteResponse.setCode(MFSSPCode.ER203.getCode());
			quoteResponse.setMessage(MFSSPCode.ER203.getMessage());

		} catch (Exception e) {
			LOGGER.error("Exception in GetQuoteServiceImpl " + e);
			quoteResponse = new GetQuoteResponseDto();
			quoteResponse.setCode(MFSSPCode.ER203.getCode());
			quoteResponse.setMessage(MFSSPCode.ER203.getMessage());

		}

		return quoteResponse;
	}

	private void logQuote(GetQuoteRequestDto request) throws DaoException {
		SPGetQuote spGetQuote = null;

		spGetQuote = transactionDao.getQuoteObject(request);
		if (spGetQuote == null) {
			spGetQuote = new SPGetQuote();
			spGetQuote.setSourceCountry(request.getSourceCountry());
			spGetQuote.setDestinationCountry(request.getDestinationCountry());
			spGetQuote.setDestinationCurrency(request.getDestinationCurrency());
			spGetQuote.setDeliveryMethod(request.getDeliveryMethod());
			spGetQuote.setDeliveryTime(request.getDeliveryTime());
			spGetQuote.setPaymentMethod(request.getPaymentMethod());
			spGetQuote.setAmount(request.getAmount());

			transactionDao.save(spGetQuote);

		} else {
			spGetQuote.setSourceCountry(request.getSourceCountry());
			spGetQuote.setDestinationCountry(request.getDestinationCountry());
			spGetQuote.setDestinationCurrency(request.getDestinationCurrency());
			spGetQuote.setDeliveryMethod(request.getDeliveryMethod());
			spGetQuote.setDeliveryTime(request.getDeliveryTime());
			spGetQuote.setPaymentMethod(request.getPaymentMethod());
			spGetQuote.setAmount(request.getAmount());

			transactionDao.update(spGetQuote);
		}

	}

	public void updateGetQuote(GetQuoteRequestDto request, GetQuoteResponseDto response) throws DaoException {
		SPGetQuote spGetQuote;

		spGetQuote = transactionDao.getQuoteObject(request);
		spGetQuote.setRate(response.getRate());
		spGetQuote.setFee(response.getFee());
		spGetQuote.setDestinationAmount(response.getDestinationAmount());
		spGetQuote.setSourceAmount(response.getSourceAmount());
		spGetQuote.setTax(response.getTax());
		spGetQuote.setAmountToPay(response.getAmountToPay());
		spGetQuote.setQuoteReference(response.getQuoteReference());
		spGetQuote.setMessage(response.getMessage());

		transactionDao.update(spGetQuote);
	}

}