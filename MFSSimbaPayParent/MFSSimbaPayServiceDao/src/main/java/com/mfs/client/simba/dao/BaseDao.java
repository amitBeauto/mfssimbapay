package com.mfs.client.simba.dao;

import com.mfs.client.simba.exception.DaoException;

public interface BaseDao {

	public boolean save(Object obj) throws DaoException;

	public boolean update(Object obj) throws DaoException;

	public boolean saveOrUpdate(Object obj) throws DaoException;

	public boolean delete(Object obj) throws DaoException;
}
