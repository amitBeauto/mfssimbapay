package com.mfs.client.simba.dto;

public class GetQuoteRequestDto {

	private String sourceCountry;
	private String destinationCountry;
	private String destinationCurrency;
	private String deliveryMethod;
	private String deliveryTime;
	private String paymentMethod;
	private double Amount;

	public String getSourceCountry() {
		return sourceCountry;
	}

	public void setSourceCountry(String sourceCountry) {
		this.sourceCountry = sourceCountry;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getDestinationCurrency() {
		return destinationCurrency;
	}

	public void setDestinationCurrency(String destinationCurrency) {
		this.destinationCurrency = destinationCurrency;
	}

	public String getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	@Override
	public String toString() {
		return "GetQuoteRequestDto [sourceCountry=" + sourceCountry + ", destinationCountry=" + destinationCountry
				+ ", destinationCurrency=" + destinationCurrency + ", deliveryMethod=" + deliveryMethod
				+ ", deliveryTime=" + deliveryTime + ", paymentMethod=" + paymentMethod + ", Amount=" + Amount + "]";
	}

}
