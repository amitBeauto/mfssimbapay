package com.mfs.client.simba.util;

public class CommonConstant {

	public static final String BASE_URL = "base_url";
	public static final String RATE_URL = "rate_url";
	public static final String QUOTE_URL = "quote_url";
	public static final String BANK_URL = "bank_url";
	public static final String INIT_URL = "init_url";
	public static final String COMMIT_URL = "commit_url";
	public static final String GET_TRANS_URL = "get_trans_url";
	public static final String GET_STATUS_URL = "get_status_url";

	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String AUTHORIZATION = "Authorization";
	public static final String NONCE = "nonce";
	public static final String SIGNATURE = "signature";
	public static final String COMPANY_ID = "company_id";
	public static final String SECRET = "secret";
	public static final String COMPANYID = "companyID";

	public static final String INVALID_QUOTE_REFERENCE = "Quote Reference is invalid";
	public static final String INVALID_AGENT_REFERENCE = "Agent Reference is invalid";
	public static final String INVALID_CALLBACK_URL = "Callback Url is invalid";
	public static final String INVALID_SENDER_FIRST_NAME = "Sender firstname is invalid";
	public static final String INVALID_SENDER_LAST_NAME = "Sender lastname is invalid";
	public static final String INVALID_SENDER_MOBILE = "Sender mobile is invalid";
	public static final String INVALID_SENDER_DOB = "Sender Date of Birth is invalid";
	public static final String INVALID_SENDER_ADDRESS = "Sender address is invalid";
	public static final String INVALID_SENDER_CITY = "Sender city is invalid";
	public static final String INVALID_SENDER_POSTCODE = "Sender postcode is invalid";
	public static final String INVALID_SENDER_COUNTRY = "Sender country is invalid";

	public static final String INVALID_RECEIVER_FIRST_NAME = "Receiver firstname is invalid";
	public static final String INVALID_RECEIVER_LAST_NAME = "Receiver lastname is invalid";
	public static final String INVALID_RECEIVER_MOBILE = "Receiver mobile number is invalid";
	public static final String INVALID_RECEIVER_ACCOUNT_NO = "Receiver Account number is invalid";
	public static final String INVALID_RECEIVER_BANK_CODE = "Receiver bank code is invalid";
	public static final String INVALID_RECEIVER_COUNTRY = "Receiver country is invalid";

	public static final String SAVE_STATUS = "Initiated";
	public static final String SAVE_STATUS_MSG = "Transaction Initiated";
	public static final String UPDATE_STATUS = "Transaction Successfull";
	public static final String ERROR_STATUS = "Transaction Error";
	public static final String ENTER_BOTH_PARAMETERS = "Enter both the parameters";
	public static final String INVALID_GET_TRANS_REQUEST = "Need to enter at least One parameter agentReference or transactionReference";
	public static final String INVALID_STATUS_REQUEST = "Invalid request Parameters";
	public static final String INVALID_SOURCE_COUNTRY = "Source country is invalid";
	public static final String INVALID_DESTINATION_COUNTRY = "Destination country is invalid";
	public static final String INVALID_DESTINATION_CURRENCY = "Invalid Destination currency";
	public static final String INVALID_DELIVERY_METHOD = "Invalid Delivery method";
	public static final String INVALID_DELIVERY_TIME = "Invalid Delivery time";
	public static final String INVALID_AMOUNT = "Invalid amount";
	public static final String INVALID_PAYMENT_METHOD = "Invalid Payment method";
	public static final String INVALID_ID_TYPE = "Invalid Id Type";
	public static final String INVALID_ID_NUMBER = "Invalid ID Number";
	public static final String INVALID_ISSUED_BY = "Invalid Issued By Field";
	public static final String INVALID_START_DATE = "Invalid Start Date for ID";
	public static final String INVALID_EXPIRY_DATE = "Invalid Expiry Date for ID";

}
