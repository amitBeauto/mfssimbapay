package com.mfs.client.simba.dto;

public class InitTransactionRequestDto {
	
	private Transaction transaction;

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public String toString() {
		return "InitTransactionRequestDto [transaction=" + transaction + "]";
	}
	
}
