package com.mfs.client.simba.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.mfs.client.simba.dao.LogTransSessionDao;
import com.mfs.client.simba.model.SPTransSession;
import com.mfs.client.simba.service.BaseService;

public class BaseServiceImpl implements BaseService {

	@Autowired
	LogTransSessionDao logTransSessionDao;

	public void logTransSession(String quoteReference, String mfsTransId, String transactionReference,
			String callbackUrl, String senderFirstName, String senderLastName, String senderMobile,
			String senderPostcode, String senderCountry, String recipientFirstName, String recipientLastName,
			String recipientMobile, String recipientCountry, String accountNumber, String bankCode, String status,
			String message, Date dateLogged, String type, String number, String issuedBy, String startDate,
			String expiryDate) {

		SPTransSession spTransSession = new SPTransSession();

		spTransSession.setQuoteReference(quoteReference);
		spTransSession.setMfsTransId(mfsTransId);
		spTransSession.setTransactionReference(transactionReference);
		spTransSession.setCallBackUrl(callbackUrl);
		spTransSession.setSenderFirstName(senderFirstName);
		spTransSession.setSenderLastName(senderLastName);
		spTransSession.setSenderMobile(senderMobile);
		spTransSession.setSenderPostcode(senderPostcode);
		spTransSession.setSenderCountry(senderCountry);
		spTransSession.setRecipientFirstName(recipientFirstName);
		spTransSession.setRecipientLastName(recipientLastName);
		spTransSession.setRecipientMobile(recipientMobile);
		spTransSession.setRecipientCountry(recipientCountry);
		spTransSession.setAccountNumber(accountNumber);
		spTransSession.setBankCode(bankCode);
		spTransSession.setStatus(status);
		spTransSession.setMessage(message);
		spTransSession.setDate(dateLogged);
		spTransSession.setType(type);
		spTransSession.setNumber(number);
		spTransSession.setIssuedBy(issuedBy);
		spTransSession.setStartDate(startDate);
		spTransSession.setExpiryDate(expiryDate);
		
		logTransSessionDao.logTransSession(spTransSession);

	}

}
