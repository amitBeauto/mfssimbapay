package com.mfs.client.simba.service.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dao.TransactionDao;
import com.mfs.client.simba.dto.GetStatusRequestDto;
import com.mfs.client.simba.dto.GetStatusResponseDto;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPTransactionLog;
import com.mfs.client.simba.service.TransactionStatusService;
import com.mfs.client.simba.util.MFSSPCode;

@Service("TransactionStatusService")
public class TransactionStatusServiceImpl implements TransactionStatusService {

	@Autowired
	SystemConfigDetailsDao systemConfigDetailsDao;

	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(TransactionStatusServiceImpl.class);

	public GetStatusResponseDto getStatus(GetStatusRequestDto request) {
		LOGGER.info("==>In TransactionStatusServiceImpl in function getStatus =" + request);
		GetStatusResponseDto response = null;

		try {

			Map<String, String> systemConfig = systemConfigDetailsDao.getConfigDetailsMap();
			if (systemConfig == null) {
				throw new Exception("No Config Details in DB");
			}

			if ((request.getStatus() != "" && request.getStatus() != null)) {
				logStatus(request);
				response = new GetStatusResponseDto();
				response.setStatus(request.getStatus());

			} else {
				response = new GetStatusResponseDto();
				response.setStatus(request.getStatus());

			}

		} catch (DaoException de) {
			LOGGER.error("==>In TransactionStatusServiceImpl in  function getStatus =" + de);
			response = new GetStatusResponseDto();
			response.setStatus(de.getStatus().getStatusCode() + de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>In TransactionStatusServiceImpl in  function getStatus =" + e);
			response.setStatus((MFSSPCode.ER203.getCode() + MFSSPCode.ER203.getMessage()));
		}
		return response;

	}

	private void logStatus(GetStatusRequestDto request) throws DaoException {
		if (request.getTransactionReference() != "" || request.getTransactionReference() != null) {

			SPTransactionLog transLog = transactionDao.getTransactionByTransReference(request);
			if (transLog != null) {
				transLog.setStatus(request.getStatus());
				transLog.setMessage(request.getMessage());
				transactionDao.update(transLog);
			}

		} else if (request.getAgentReference() != "" || request.getAgentReference() != null) {

			SPTransactionLog transLog = transactionDao.getTransationByTransId(request.getAgentReference());
			if (transLog != null) {
				transLog.setStatus(request.getStatus());
				transLog.setMessage(request.getMessage());
				transactionDao.update(transLog);
			}

		}
	}
}
