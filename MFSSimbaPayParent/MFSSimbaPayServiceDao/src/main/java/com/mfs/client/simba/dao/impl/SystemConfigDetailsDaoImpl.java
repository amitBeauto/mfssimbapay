package com.mfs.client.simba.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.simba.dao.SystemConfigDetailsDao;
import com.mfs.client.simba.dto.ResponseStatus;
import com.mfs.client.simba.exception.DaoException;
import com.mfs.client.simba.model.SPSystemConfig;
import com.mfs.client.simba.util.MFSSPCode;

@EnableTransactionManagement
@Repository("SystemConfigDetailsDao")
public class SystemConfigDetailsDaoImpl implements SystemConfigDetailsDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	private static final Logger LOGGER = Logger.getLogger(SystemConfigDetailsDaoImpl.class);
	
	@Transactional
	public Map<String, String> getConfigDetailsMap()throws DaoException {
		
		Map<String, String> spSystemConfigurationMap = null;
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "From SPSystemConfig";
		Query query = session.createQuery(hql);
		try
		{
			List<SPSystemConfig> spSystemConfigurationList = query.list();
		
			if (spSystemConfigurationList != null && !spSystemConfigurationList.isEmpty()) {
				spSystemConfigurationMap = new HashMap<String, String>();
				for (SPSystemConfig spSystemConfiguration : spSystemConfigurationList) {
					spSystemConfigurationMap.put(spSystemConfiguration.getConfigKey(),
							spSystemConfiguration.getConfigValue());
				}
			}
		}
		catch (Exception e) {
			
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER207.getCode());
			status.setStatusMessage(MFSSPCode.ER207.getMessage());
			throw new DaoException(status);
		}
		return spSystemConfigurationMap;
	}

}
