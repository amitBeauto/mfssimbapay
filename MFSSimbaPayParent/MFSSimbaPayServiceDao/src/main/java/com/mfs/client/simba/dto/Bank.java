package com.mfs.client.simba.dto;

public class Bank {
	
	private String accountNumber;
	private String bankCode; 
	private String branchCode; 
	private String country;
	
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString() {
		return "Bank [accountNumber=" + accountNumber + ", bankCode=" + bankCode + ", branchCode=" + branchCode
				+ ", country=" + country + "]";
	}
	
}
