package com.mfs.client.simba.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sp_time")
public class SPTime {
	@Id
	@GeneratedValue
	@Column(name = "trans_time_id")
	private long transTimeId;

	@Column(name = "time_code")
	private String timeCode;

	@ManyToOne
	@JoinColumn(name = "sp_get_rate_id")
	private SPGetRates spGetRates;

	public long getTransTimeId() {
		return transTimeId;
	}

	public void setTransTimeId(long transTimeId) {
		this.transTimeId = transTimeId;
	}

	public String getTimeCode() {
		return timeCode;
	}

	public void setTimeCode(String timeCode) {
		this.timeCode = timeCode;
	}

	public SPGetRates getSpGetRates() {
		return spGetRates;
	}

	public void setSpGetRates(SPGetRates spGetRates) {
		this.spGetRates = spGetRates;
	}

	@Override
	public String toString() {
		return "SPTime [transTimeId=" + transTimeId + ", timeCode=" + timeCode + ", spGetRates=" + spGetRates + "]";
	}

}
