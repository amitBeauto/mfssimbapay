package com.mfs.client.simba.service;

import com.mfs.client.simba.dto.GetTransactionRequestDto;
import com.mfs.client.simba.dto.GetTransactionResponseDto;

public interface GetTransactionService {

	GetTransactionResponseDto getTransaction(GetTransactionRequestDto request);

}
