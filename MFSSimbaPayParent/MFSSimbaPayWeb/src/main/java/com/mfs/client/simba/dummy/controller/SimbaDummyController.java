package com.mfs.client.simba.dummy.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.simba.dto.Banks;
import com.mfs.client.simba.dto.Branch;
import com.mfs.client.simba.dto.Delivery;
import com.mfs.client.simba.dto.Fee;
import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetBankResponseDto;
import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetQuoteResponseDto;
import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetRateResponseDto;
import com.mfs.client.simba.dto.GetStatusRequestDto;
import com.mfs.client.simba.dto.GetStatusResponseDto;
import com.mfs.client.simba.dto.GetTransactionRequestDto;
import com.mfs.client.simba.dto.GetTransactionResponseDto;
import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;
import com.mfs.client.simba.dto.Payment;
import com.mfs.client.simba.dto.Rates;
import com.mfs.client.simba.dto.Time;

@RestController
public class SimbaDummyController {

	@RequestMapping("/rate/response")
	public GetRateResponseDto getRateDummy(@RequestBody GetRateRequestDto request) {
		GetRateResponseDto response = new GetRateResponseDto();
		Rates rates = new Rates();
		Delivery delivery = new Delivery();
		Payment payment = new Payment();
		Payment payment2 = new Payment();
		Time time = new Time();
		Fee fee = new Fee();
		Fee fee2 = new Fee();
		Fee fee3 = new Fee();
		Fee fee4 = new Fee();
		List<Time> timeList = new ArrayList<Time>();
		List<Fee> feeList = new ArrayList<Fee>();
		List<Fee> feeList2 = new ArrayList<Fee>();
		List<Rates> ratesList = new ArrayList<Rates>();
		List<Payment> paymentList = new ArrayList<Payment>();
		List<Delivery> deliveryList = new ArrayList<Delivery>();

		time.setTimeCode("ind");
		timeList.add(time);

		fee.setAmount(50.0);
		fee.setDeliveryTime("EXP");
		fee.setLowerBound(500.0);
		fee.setUpperBound(5000.0);

		fee2.setAmount(100.5);
		fee2.setDeliveryTime("ORD");
		fee2.setLowerBound(5000.0);
		fee2.setUpperBound(100000.0);
		
		fee3.setAmount(80);
		fee3.setDeliveryTime("ORD");
		fee3.setLowerBound(4000.0);
		fee3.setUpperBound(100000.0);
		
		fee4.setAmount(200);
		fee4.setDeliveryTime("EXP");
		fee4.setLowerBound(5000.0);
		fee4.setUpperBound(100000.0);
		
		feeList.add(fee);
		feeList.add(fee2);
		feeList2.add(fee3);
		feeList2.add(fee4);

		payment.setFees(feeList);
		payment.setMaximum(10000);
		payment.setMethod("MOB");
		payment.setMinimum(500.2);
		

		payment2.setFees(feeList2);
		payment2.setMaximum(10000);
		payment2.setMethod("BANK");
		payment2.setMinimum(500);

		delivery.setAmounts(1000);
		delivery.setMinimum(500);
		delivery.setMaximum(100000);
		delivery.setMethod("MOB");
		delivery.setRate(100);

		paymentList.add(payment);
		paymentList.add(payment2);
		deliveryList.add(delivery);

		rates.setDestinationCountry(request.getDestinationCountry());
		rates.setDestinationCurrency("frank");
		rates.setSourceCurrency("inr");
		rates.setPayments(paymentList);
		rates.setDeliveries(deliveryList);
		rates.setTimes(timeList);
		ratesList.add(rates);

		response.setRates(ratesList);

		return response;
	}

	@RequestMapping(value = "/transaction/quote")
	public GetQuoteResponseDto getQuote(@RequestBody GetQuoteRequestDto request) {
		GetQuoteResponseDto response = new GetQuoteResponseDto();

		response.setRate(200);
		response.setFee(50);
		response.setDestinationAmount(1000);
		response.setSourceAmount(1100);
		response.setTax(100);
		response.setAmountToPay(900);
		response.setQuoteReference("abc");
		response.setMessage("successful");

		return response;
	}

	@RequestMapping(value = "/transaction/initiate")
	public InitTransactionResponseDto initTransaction(@RequestBody InitTransactionRequestDto request,
			@RequestHeader HttpHeaders httpHeader) {
		InitTransactionResponseDto response = new InitTransactionResponseDto();
		String signature = "f164d7c34c7f23af8f103fdd2a5f055bdc863c56952e799d0e1b6674cef0b24c";
		// String signature = "";
		Map<String, String> map = httpHeader.toSingleValueMap();
		String sign = map.get("signature");

		if (sign.equals(signature)) {
			response.setQuoteReference("Quote101");
			response.setStatus("OK");
			response.setMessage("");
		} else {
			response.setCode("ER301");
			response.setMessage("Autorization Failed");
			response.setStatus("Fail");
		}
		return response;
	}

	@RequestMapping(value = "/transaction/commit")
	public InitTransactionResponseDto commitTransaction(@RequestBody String quoteRef) {
		InitTransactionResponseDto response = new InitTransactionResponseDto();
		response.setTransactionReference("TrRef101");
		;
		response.setStatus("COMPLETE");
		response.setMessage("");
		return response;
	}

	@RequestMapping(value = "/transaction/get")
	public GetTransactionResponseDto getTransaction(@RequestBody GetTransactionRequestDto request) {
		GetTransactionResponseDto response = new GetTransactionResponseDto();
		response.setTransactionReference("ppp");
		response.setStatus("OK213");
		response.setMessage("");

		return response;
	}

	@RequestMapping("/banks/get")
	public GetBankResponseDto bankDummyCntrl(@RequestBody GetBankRequestDto request) {
		GetBankResponseDto response = new GetBankResponseDto();
		Banks res = new Banks();
		Branch br = new Branch();
		Branch br1 = new Branch();

		List<Branch> branchList = new ArrayList<Branch>();
		List<Banks> bankList = new ArrayList<Banks>();
		branchList.add(br);
		branchList.add(br1);

		res.setCode("222");
		res.setName("boom");
		res.setCountry(request.getCountry());
		res.setBranches(branchList);

		response.setBanks(bankList);

		br.setCode("300");
		br.setName("Pune");

		br1.setCode("102");
		br1.setName("Pune");

		bankList.add(res);

		return response;
	}

	@RequestMapping(value = "/transaction/status")
	public GetStatusResponseDto getStatus(@RequestBody GetStatusRequestDto request) {
		GetStatusResponseDto response = new GetStatusResponseDto();
		response.setStatus("OK");
		return response;

	}

}
