package com.mfs.client.simba.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.simba.dto.GetBankRequestDto;
import com.mfs.client.simba.dto.GetBankResponseDto;
import com.mfs.client.simba.dto.GetQuoteRequestDto;
import com.mfs.client.simba.dto.GetQuoteResponseDto;
import com.mfs.client.simba.dto.GetRateRequestDto;
import com.mfs.client.simba.dto.GetRateResponseDto;
import com.mfs.client.simba.dto.GetStatusRequestDto;
import com.mfs.client.simba.dto.GetStatusResponseDto;
import com.mfs.client.simba.dto.GetTransactionRequestDto;
import com.mfs.client.simba.dto.GetTransactionResponseDto;
import com.mfs.client.simba.dto.InitTransactionRequestDto;
import com.mfs.client.simba.dto.InitTransactionResponseDto;
import com.mfs.client.simba.dto.ResponseStatus;
import com.mfs.client.simba.service.CommitTransactionService;
import com.mfs.client.simba.service.GetBanksService;
import com.mfs.client.simba.service.GetQuoteService;
import com.mfs.client.simba.service.GetRatesService;
import com.mfs.client.simba.service.GetTransactionService;
import com.mfs.client.simba.service.InitiateTransactionService;
import com.mfs.client.simba.service.TransactionStatusService;
import com.mfs.client.simba.util.CommonConstant;
import com.mfs.client.simba.util.CommonValidations;
import com.mfs.client.simba.util.MFSSPCode;
import com.mfs.client.simba.util.ValidateRequest;

@RestController
public class SimbaController {

	@Autowired
	GetRatesService getRatesService;

	@Autowired
	GetQuoteService getQuoteService;

	@Autowired
	InitiateTransactionService initiateTransactionService;
	
	@Autowired
	CommitTransactionService commitTransactionService;

	@Autowired
	GetTransactionService getTransactionService;

	@Autowired
	GetBanksService getBankService;

	@Autowired
	TransactionStatusService transactionStatusService;

	private static final Logger LOGGER = Logger.getLogger(SimbaController.class);

	@RequestMapping(value = "/getrates", method = RequestMethod.POST)
	@ResponseBody
	public GetRateResponseDto getRates(@RequestBody GetRateRequestDto getRateRequest) {

		LOGGER.info("==>In SimbaController in getRates function getRates =" + getRateRequest);
		GetRateResponseDto getRateResponse = null;

		if (getRateRequest != null) {
			getRateResponse = getRatesService.getRates(getRateRequest);
			if (getRateResponse == null) {
				getRateResponse = new GetRateResponseDto();
				ResponseStatus status = new ResponseStatus();
				status.setStatusCode(MFSSPCode.ER203.getCode());
				status.setStatusMessage("Response is received null");
				getRateResponse.setResponseStatus(status);

			}
		} else {
			getRateResponse = new GetRateResponseDto();
			ResponseStatus response = new ResponseStatus();
			response.setStatusCode(MFSSPCode.ER203.getCode());
			response.setStatusMessage("request received is null");
			getRateResponse.setResponseStatus(response);
		}
		LOGGER.info("Inside SimbaController --->getRates Response" + getRateResponse);
		return getRateResponse;
	}

	@RequestMapping(value = "/getquote", method = RequestMethod.POST)
	@ResponseBody
	public GetQuoteResponseDto getQuote(@RequestBody GetQuoteRequestDto request) {
		CommonValidations validation = new CommonValidations();
		GetQuoteResponseDto response = null;
		if (request != null) {
			if (validation.validateStringValues(request.getSourceCountry())) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SOURCE_COUNTRY);
			} else if (validation.validateStringValues(request.getDestinationCountry())) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_DESTINATION_COUNTRY);

			} else if (validation.validateStringValues(request.getDestinationCurrency())) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_DESTINATION_CURRENCY);

			} else if (validation.validateStringValues(request.getDeliveryMethod())) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_DELIVERY_METHOD);

			} else if (validation.validateStringValues(request.getDeliveryTime())) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_DELIVERY_TIME);

			} else if (validation.validateAmountValues(request.getAmount())) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_AMOUNT);

			} else if (validation.validateStringValues(request.getPaymentMethod())) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_PAYMENT_METHOD);

			} else

				response = getQuoteService.getQuote(request);
			if (response == null) {
				response = new GetQuoteResponseDto();
				response.setCode(MFSSPCode.ER206.getCode());
				response.setMessage(MFSSPCode.ER206.getMessage());
			}
		} else {
			response = new GetQuoteResponseDto();
			response.setCode(MFSSPCode.ER220.getCode());
			response.setMessage(MFSSPCode.ER220.getMessage());

		}
		return response;
	}

	@RequestMapping(value = "/init")
	@ResponseBody
	public InitTransactionResponseDto initTransaction(@RequestBody InitTransactionRequestDto request) {
		InitTransactionResponseDto response = null;
		ValidateRequest validate = new ValidateRequest();

		if (request != null) {
			response = validate.validateInitRequest(request);
			if (response == null) {
				response = initiateTransactionService.initTransaction(request);
				if (response == null) {
					response = new InitTransactionResponseDto();
					response.setCode(MFSSPCode.ER203.getCode());
					response.setMessage(MFSSPCode.ER203.getMessage());
				}
			}
		} else {
			response = new InitTransactionResponseDto();
			response.setCode(MFSSPCode.ER213.getCode());
			response.setMessage(MFSSPCode.ER213.getMessage());
		}

		return response;
	}
	
	
	@RequestMapping(value = "/getTransaction")
	@ResponseBody
	public GetTransactionResponseDto getTransaction(@RequestBody GetTransactionRequestDto request) throws Exception {

		LOGGER.info("Inside SimbaController in GetTransaction function" + request);
		CommonValidations validation = new CommonValidations();
		GetTransactionResponseDto response = null;

		if (request != null) {
			if (validation.validateStringValues(request.getTransactionReference())
					&& validation.validateStringValues(request.getAgentReference())) {

				response = new GetTransactionResponseDto();
				response.setStatus(MFSSPCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSSPCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_GET_TRANS_REQUEST);
			} else {
				response = getTransactionService.getTransaction(request);
				if (response == null) {
					response = new GetTransactionResponseDto();
					response.setStatus(MFSSPCode.ER203.getCode());
					response.setMessage(MFSSPCode.ER203.getMessage());
				}

			}

		} else {

			response = new GetTransactionResponseDto();
			response.setStatus(MFSSPCode.ER213.getCode());
			response.setMessage(MFSSPCode.ER213.getMessage());

		}

		LOGGER.info("Inside SimbaController --->GetTransaction Response" + response);
		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/getbanks", method = RequestMethod.POST)
	public GetBankResponseDto getBankDetails(@RequestBody GetBankRequestDto request) {

		LOGGER.info("Inside SimbaController in getBankDetails function" + request);
		GetBankResponseDto response = null;

		if (request != null) {
			response = getBankService.getBankDetails(request);

			if (response == null) {
				response = new GetBankResponseDto();
				ResponseStatus status = new ResponseStatus();
				status.setStatusCode(MFSSPCode.ER203.getCode());
				status.setStatusMessage(MFSSPCode.ER203.getMessage());
				response.setResponseStatus(status);
			}
		} else {
			response = new GetBankResponseDto();
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSSPCode.ER220.getCode());
			status.setStatusMessage(MFSSPCode.ER220.getMessage());
			response.setResponseStatus(status);
		}
		LOGGER.info("Inside SimbaController --->GetBank Response" + response);

		return response;
	}

	@ResponseBody
	@RequestMapping(value = "/pushnotify", method = RequestMethod.POST)
	public GetStatusResponseDto pushNotification(@RequestBody GetStatusRequestDto request) {

		LOGGER.info("==>In SimbaController in function pushNotification =" + request);
		GetStatusResponseDto response = null;
		if (request != null) {
			if (((request.getAgentReference() != null && request.getAgentReference() != "")
					|| (request.getTransactionReference() != null && request.getTransactionReference() != ""))
					&& (request.getStatus() != null && request.getStatus() != "")) {
				response = transactionStatusService.getStatus(request);

			} else {
				response = new GetStatusResponseDto();
				response.setStatus(MFSSPCode.ER203.getMessage() + " " + CommonConstant.INVALID_STATUS_REQUEST);

			}
		} else {
			response = new GetStatusResponseDto();
			response.setStatus(MFSSPCode.ER203.getMessage() + " " + CommonConstant.INVALID_STATUS_REQUEST);
		}
		return response;

	}

}
